from django.urls import reverse

from .bne_base import BneBaseTest
from ..models import Reward, Topic, Challenge, ChallengeProgress, ChallengeParticipation


class RewardTestCase(BneBaseTest):

    def setUp(self):
        super().setUp()
        # Create Topics and Challenges
        tpc1 = Topic(internal_id=1, topic_name='Topic 1', image_level1='img_level1.png')
        tpc2 = Topic(internal_id=2, topic_name='Topic 2', image_level1='img_level2.png')
        self.supply_objects(tpc1, tpc2)

        clg1 = Challenge(title='Challenge 1', description='Desc 1', icon='icon1.png', image='img1.png', duration=1,
                         color='111111', difficulty=1, periodicity=1, topic=tpc1)
        clg2 = Challenge(title='Challenge 2', description='Desc 2', icon='icon2.png', image='img2.png', duration=2,
                         color='222222', difficulty=2, periodicity=1, topic=tpc2)
        self.supply_objects(clg1, clg2)

    def createRewards(self, cond):
        reward = Reward(type="MEDAL", color="#121212", points=10, condition=cond)
        reward1 = Reward(type="TROPHY", color="#121212", points=10, condition=cond)
        self.supply_objects(reward, reward1)

        return [reward.pk, reward1.pk]

    def createParticipation(self, title):
        clg = Challenge.objects.filter(title=title)[0]
        return self.client.post(reverse('challenge-participation', args=[clg.pk]), format='json')

    def createProgress(self, clg):
        return self.client.post(reverse('challenge-progress', args=[clg.pk]), format='json')

    def test_noReward(self):
        # Reward that is not going to be achieved
        self.createRewards(2)
        participationResponse = self.createParticipation('Challenge 1')
        participation = ChallengeParticipation.objects.first()
        self.createProgress(participation)

        self.assertEqual(201, participationResponse.status_code)
        self.assertFalse(participationResponse.has_header('X-Achieved-Reward'))

    def test_oneReward(self):
        # Reward that is going to be achieved
        rewards = self.createRewards(1)
        participationResponse = self.createParticipation('Challenge 1')
        c1 = Challenge.objects.filter(title='Challenge 1')[0]

        self.assertEqual(201, participationResponse.status_code)
        self.assertFalse(participationResponse.has_header('X-Achieved-Reward')) # No reward yet

        for i in range(0, c1.duration -1):
            progressResponse = self.createProgress(c1)
            self.assertFalse(progressResponse.has_header('X-Achieved-Reward')) # Not achieved yet

        progressResponse = self.createProgress(c1)  # achieved a new reward
        self.assertTrue(progressResponse.has_header('X-Achieved-Reward'))

        achievedRewards = progressResponse.__getitem__('X-Achieved-Reward').replace('[', '').replace(']', '') # get elements returned
        achievedRewards = achievedRewards.split(',')
        self.assertEqual(rewards[0], int(achievedRewards[0])) # returned achievment is the correct one
        self.assertEqual(1, len(achievedRewards)) #only one is achieved


    def test_moreRewards(self):
        # Achieve multiple rewards at one time
        participationResponse = self.createParticipation('Challenge 1')
        participationResponse = self.createParticipation('Challenge 2')
        challenges = Challenge.objects.all()

        self.assertEqual(201, participationResponse.status_code)
        self.assertFalse(participationResponse.has_header('X-Achieved-Reward'))  # No reward yet
        for c in challenges:
            for i in range(0, c.duration - 1):
                progressResponse = self.createProgress(c)
                self.assertFalse(progressResponse.has_header('X-Achieved-Reward'))  # Not achieved yet

        progressResponse = self.createProgress(challenges[0])
        rewards = self.createRewards(1) # create rewards, that is returns 2 at the same time
        progressResponse = self.createProgress(challenges[1])  # achieved the rewards

        self.assertTrue(progressResponse.has_header('X-Achieved-Reward'))
        achievedRewards = progressResponse.__getitem__('X-Achieved-Reward').replace('[', '').replace(']', '')  # get elements returned
        achievedRewards = achievedRewards.split(',')
        self.assertEqual(rewards[0], int(achievedRewards[0]))  # first achievement is correct
        self.assertEqual(rewards[1], int(achievedRewards[1]))  # second achievement is correct
        self.assertEqual(2, len(achievedRewards))  # both are achieved
