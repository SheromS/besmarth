from rest_framework import status
from rest_framework.test import APITestCase
from rest_framework.authtoken.models import Token

from ..models import Account

class BneBaseTest(APITestCase):

    # Global Variables
    img_testsrv_prefix = 'http://testserver/media/'

    def setUp(self):
        self.u_name, self.u_password, self.token= self.setupTestAccount()
        self.test_user = Account.objects.filter(username=self.u_name)[0]
        self.set_token();

    """
    Creates a test account with token for everyone to use. If The account exists previously
    that will be deleted and recreated
    return: (username / password / token)
    """
    def setupTestAccount(self):
        user_name = 'TestUser';
        user_password = '12345'

        if (Account.objects.filter(username=user_name).count() == 1):
            Account.objects.filter(username=user_name).delete();
            assert Account.objects.count() == 1;

        testAccount = Account(username=user_name, password=user_password, first_name='Hugo', last_name='Habicht',
                              email='h-h@sustainable-behaviour.com', pseudonymous=False);
        testAccount.save();
        token = Token.objects.get_or_create(user=testAccount)[0]
        return user_name, user_password, token.key;

    """
    Stores the given objects in the Testdatabase
    """
    def supply_objects(self, *objects):
        for e in objects:
            e.save()

    """
    Returns the response for a get request for the given url and checks if the given status code matches the response code.
    
    return: response
    """
    def get_response(self, url, format='json', exp_status_code=status.HTTP_200_OK):
        response = self.client.get(url, format=format)
        self.assertEquals(exp_status_code, response.status_code)
        return response

    """
    Returns the response for a POST request for the given url and checks if the given status code matches the response code.
    
    return: response
    """
    def post_response(self, url, format='json', exp_status_code=status.HTTP_201_CREATED):
        response = self.client.post(url, format=format)
        self.assertEquals(exp_status_code, response.status_code)
        return response

    """
    Returns the response for a DELETE request for the given url and checks if the given status code matches the response code.

    return: response
    """
    def delete_response(self, url, format='json', exp_status_code=status.HTTP_200_OK):
        response = self.client.delete(url, format=format)
        self.assertEquals(exp_status_code, response.status_code)
        return response

    """
    Returns the response for a PATCH request for the given url and checks if the given status code matches the response code.

    return: response
    """
    def patch_response(self, url, data, format='json', exp_status_code=status.HTTP_200_OK):
        response = self.client.patch(url, data, format=format)
        self.assertEquals(exp_status_code, response.status_code)
        return response

    """
    Sets the default token if no ther is specified. This method must be invoked, if the token is not set manually.
    """
    def set_token(self, token='other'):

        if token != 'other':
            self.client.credentials(HTTP_AUTHORIZATION='Token ' + token)
        else:
            self.client.credentials(HTTP_AUTHORIZATION='Token ' + self.token)