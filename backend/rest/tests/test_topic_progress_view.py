from django.urls import reverse
from rest_framework import status
from django.utils import timezone

from .bne_base import BneBaseTest
from ..models import Topic, Challenge, ChallengeParticipation, ChallengeProgress, TopicLevel


class TestTopicProgressView(BneBaseTest):

    def createTopicLevels(self):
        self.topic_level1 = TopicLevel(name="Recruit", limit=10)
        self.topic_level2 = TopicLevel(name="Intermediate", limit=100)
        self.topic_level3 = TopicLevel(name="Expert", limit=1000)
        self.topic_level0 = TopicLevel(name="Max Level", limit=-1)
        self.supply_objects(self.topic_level1, self.topic_level2, self.topic_level3, self.topic_level0)

    def createProgress(self, participation, amount):
        """Create progress for a challenge participation"""
        for i in range(amount):
            progress = ChallengeProgress(create_time=timezone.now(), mark_deleted=False,
                                         participation_id=participation.id)
            progress.save()

    def createChallenge(self, topic, name, difficulty, duration, progress):
        """
        Creates a challenge as well as challengeparticipation for self.test_user and a certain
        amount of challengeprogress, as specified in `progress` parameter.

        topic      := topic model instance to which this challenge relates
        name       := challenge name
        difficulty := integer value between [0,2]
        duration   := challenge duration (defines the amount of progress until the challenge is finished for once)
        progress   := number of progress to generate
        """
        # color, periodicity and category are irrelevant and therefore statically typed
        c = Challenge(title=name, duration=duration, color="#FFFFFF",
                      difficulty=Challenge.DIFFICULTY_CHOICES[difficulty][0],
                      periodicity=Challenge.PERIODICITY_CHOICES[0][0],
                      topic=topic,
                      category=Challenge.CATEGORY_CHOICES[0][0])
        c.save()
        cp = ChallengeParticipation(joined_time=timezone.now(), shared=False, challenge_id=c.id,
                                    user_id=self.test_user.id, mark_deleted=False)
        cp.save()
        self.createProgress(cp, progress)

    def setUp(self):
        super().setUp()
        # internal_id=0 is defined as "Forest" as of writing of this test. But it does not matter at all,
        # as long as the number is valid/accepted.
        self.topic1 = Topic(internal_id=0, topic_name="Topic1")
        self.topic2 = Topic(internal_id=1, topic_name="Topic2")
        self.supply_objects(self.topic1, self.topic2)


    def test_topic_not_exists(self):
        # presumed 12345 is a non existing topic id
        topic_progress_url = reverse('topic-progress', args=[12345])
        self.get_response(topic_progress_url, exp_status_code=status.HTTP_404_NOT_FOUND)

    def test_no_topic_levels_specified(self):
        """
        Test whether the application manages to deal with a situation where
        the user did not enter any TopicLevels to the database.
        """
        topic_progress_url = reverse('topic-progress', args=[self.topic1.id])
        self.get_response(topic_progress_url)

    def test_first_level(self):
        self.createTopicLevels()

        expected_score = 0
        expected_level = 1
        expected_score_prev = 0
        expected_score_next = 10

        topic_progress_url = reverse('topic-progress', args=[self.topic1.id])
        data = self.get_response(topic_progress_url).data
        self.assertEqual(data['score'], expected_score)
        self.assertEqual(data['level'], expected_level)
        self.assertEqual(data['scorePrevLevel'], expected_score_prev)
        self.assertEqual(data['scoreNextLevel'], expected_score_next)
        self.assertEqual(data['levelDescription'], self.topic_level1.name)

    def test_level_maths(self):
        self.createTopicLevels()

        # Maths should work as follows:
        # Topic Score = sum(difficulty*completed)
        # Where sum sums up all challenges from a topic, where
        # the user participated in. The score for each challenge
        # is equal to (number of times completed)*(challenge difficulty)

        # should produce a score of (20/10)*1 = 2
        self.createChallenge(self.topic1, "Challenge1", 0, 10, 20)
        # should produce a score of (23/5)*2 = 8
        self.createChallenge(self.topic1, "Challenge2", 1, 5, 23)
        # should produce a score of (8/7)*3 = 3
        self.createChallenge(self.topic1, "Challenge2", 2, 7, 8)
        # score should be ignored, since not part of topic1
        self.createChallenge(self.topic2, "Challenge3", 0, 10, 20)
        # makes a total (expected) score of 13 in topic1
        expected_score = 13
        expected_level = 2
        expected_score_prev = 10
        expected_score_next = 100

        topic_progress_url = reverse('topic-progress', args=[self.topic1.id])
        data = self.get_response(topic_progress_url).data
        self.assertEqual(data['score'], expected_score)
        self.assertEqual(data['level'], expected_level)
        self.assertEqual(data['scorePrevLevel'], expected_score_prev)
        self.assertEqual(data['scoreNextLevel'], expected_score_next)
        self.assertEqual(data['levelDescription'], self.topic_level2.name)

    def test_last_level(self):
        self.createTopicLevels()

        # should produce a score of 101
        self.createChallenge(self.topic1, "Challenge1", 0, 1, 101)

        expected_score = 101
        expected_level = 3
        expected_score_prev = 100
        expected_score_next = 1000

        topic_progress_url = reverse('topic-progress', args=[self.topic1.id])
        data = self.get_response(topic_progress_url).data
        self.assertEqual(data['score'], expected_score)
        self.assertEqual(data['level'], expected_level)
        self.assertEqual(data['scorePrevLevel'], expected_score_prev)
        self.assertEqual(data['scoreNextLevel'], expected_score_next)
        self.assertEqual(data['levelDescription'], self.topic_level3.name)

    def test_level_boundaries(self):
        """
        Test correctness when score is exactly on the boundary/limit
        of a TopicLevel. According to definition, a topic level goes
        up to < limit, that means limit is already the beginning of
        the next level.
        """
        self.createTopicLevels()

        # test first/second level boundary
        self.createChallenge(self.topic1, "Challenge1", 0, 1, 10)

        expected_score = 10
        expected_level = 2
        expected_score_prev = 10
        expected_score_next = 100

        topic_progress_url = reverse('topic-progress', args=[self.topic1.id])
        data = self.get_response(topic_progress_url).data
        self.assertEqual(data['score'], expected_score)
        self.assertEqual(data['level'], expected_level)
        self.assertEqual(data['scorePrevLevel'], expected_score_prev)
        self.assertEqual(data['scoreNextLevel'], expected_score_next)
        self.assertEqual(data['levelDescription'], self.topic_level2.name)

    def test_max_level(self):
        self.createTopicLevels()

        # should produce a score of 101
        self.createChallenge(self.topic1, "Challenge1", 0, 1, 1001)

        expected_score = 1001
        expected_level = 4
        expected_score_prev = 1000
        expected_score_next = expected_score

        topic_progress_url = reverse('topic-progress', args=[self.topic1.id])
        data = self.get_response(topic_progress_url).data
        self.assertEqual(data['score'], expected_score)
        self.assertEqual(data['level'], expected_level)
        self.assertEqual(data['scorePrevLevel'], expected_score_prev)
        self.assertEqual(data['scoreNextLevel'], expected_score_next)
        self.assertEqual(data['levelDescription'], self.topic_level0.name)

    def test_playground(self):
        topic_id = self.topic1.id
        user_id = self.test_user.id

        # This query looks for the progress of all challenges that are part of a specific topic
        # and in which the user participates in.
        # Progress is represented by the number of times a given challenge has been completed.
        # It is calculated using an integer division that means only completed challenges will
        # be reflected in the progress.
        # The query result contains: challenge_id, challenge_title, challenge_difficulty, progress
        qry = f"""
        SELECT c.id, c.title, c.difficulty, count(cprogress.id)/c.duration AS 'progress'
        FROM rest_challengeprogress cprogress
        JOIN rest_challengeparticipation cparticipation ON cprogress.participation_id = cparticipation.id 
        JOIN rest_challenge c on cparticipation.challenge_id = c.id
        JOIN rest_topic t on c.topic_id = t.id
        WHERE cparticipation.user_id={user_id}
        AND t.id={topic_id}
        GROUP BY c.id
        """

        # difficulty factors
        d_factors = {
            Challenge.DIFFICULTY_CHOICES[0][0]: 1,
            Challenge.DIFFICULTY_CHOICES[1][0]: 2,
            Challenge.DIFFICULTY_CHOICES[2][0]: 3,
        }

        score = 0
        challenge_progresses = ChallengeProgress.objects.raw(qry)
        for cp in challenge_progresses:
            # print(f"challenge={cp.title} progress={cp.progress} difficulty={cp.difficulty}")
            score += cp.progress * d_factors[cp.difficulty]
