from django.urls import reverse
from rest_framework import status

from .bne_base import BneBaseTest
from ..models import Topic, Challenge, ChallengeProgress, ChallengeParticipation


# ChallengeParticipation and ChallengeProgression are tested in the same class because their
# functionality is intended to work allways together.
class TestChallengeParticipationProgression(BneBaseTest):

    def setUp(self):
        super().setUp()

        tpc1 = Topic(internal_id=1, topic_name='Topic 1', image_level1='img_level1.png')
        tpc2 = Topic(internal_id=2, topic_name='Topic 2', image_level1='img_level2.png')
        self.supply_objects(tpc1, tpc2)

        clg1 = Challenge(title='Challenge 1', description='Desc 1', icon='icon1.png', image='img1.png', duration=4,
                         color='111111', difficulty=1, periodicity=1, topic=tpc1)
        clg2 = Challenge(title='Challenge 2', description='Desc 2', icon='icon2.png', image='img2.png', duration=2,
                         color='222222', difficulty=2, periodicity=2, topic=tpc2)

        self.supply_objects(clg1, clg2)

    def insert_demo_data(self):
        clg1 = Challenge.objects.filter(title='Challenge 1')[0]
        clg2 = Challenge.objects.filter(title='Challenge 2')[0]
        self.post_response(reverse('challenge-participation', args=[clg1.pk]))
        self.post_response(reverse('challenge-participation', args=[clg2.pk]))

        # insert progressions
        ChallengeParticipation.objects.filter(user=self.test_user, challenge=clg1)
        ChallengeParticipation.objects.filter(user=self.test_user, challenge=clg1)

        self.post_response(reverse('challenge-progress', args=[clg1.pk]))
        self.post_response(reverse('challenge-progress', args=[clg1.pk]))
        self.post_response(reverse('challenge-progress', args=[clg1.pk]))

        self.post_response(reverse('challenge-progress', args=[clg2.pk]))

        return clg1, clg2

    def validate_participation_response(self, response):
        properties = ['id', 'joined_time', 'challenge', 'mark_deleted']
        for p in properties:
            self.assertTrue(p in response.data, "Response does not contain: {}".format(p))

    def test_create_participation(self):

        # Check: Create participation
        challenge = Challenge.objects.filter(title='Challenge 1')[0]
        url = reverse('challenge-participation', args=[challenge.pk])
        response = self.post_response(url)
        self.assertEquals(1, ChallengeParticipation.objects.count())
        self.assertEquals(1, ChallengeParticipation.objects.filter(challenge=challenge).count())

        # Check response contains valid properties
        self.validate_participation_response(response)

        # Check: Create participation with different challenge
        challenge = Challenge.objects.filter(title='Challenge 2')[0]
        url = reverse('challenge-participation', args=[challenge.pk])
        self.post_response(url)
        self.assertEquals(2, ChallengeParticipation.objects.count())
        self.assertEquals(1, ChallengeParticipation.objects.filter(challenge=challenge).count())

        # Check: Second creation is not possible
        self.post_response(url, exp_status_code=status.HTTP_400_BAD_REQUEST)

        # Check: Create a participation with an invalid challenge
        url = reverse('challenge-participation', args=[1000])
        self.post_response(url, exp_status_code=status.HTTP_404_NOT_FOUND)

    def test_patch_participation(self):
        # insert demo data
        clg1, clg2 = self.insert_demo_data()
        clg1.shared = False
        clg1.save()
        url = reverse('challenge-participation', args=[clg1.pk])
        data = {'shared': True}
        response = self.patch_response(url, data)
        participation = ChallengeParticipation.objects.get(user=self.test_user, challenge=clg1)
        self.assertEquals(response.data['challenge']['title'], clg1.title)
        self.assertEquals(response.data['mark_deleted'], False)
        self.assertEquals(response.data['shared'], True)
        self.assertEquals(len(response.data['progress']), 3)

    def test_get_participation(self):

        # insert demo data
        clg1, clg2 = self.insert_demo_data()

        # Setup get Urls
        url1 = reverse('challenge-participation', args=[clg1.pk])
        url2 = reverse('challenge-participation', args=[clg2.pk])

        # Check: Get different participations
        response1 = self.get_response(url1)
        response2 = self.get_response(url2)

        self.assertEquals(3, len(response1.data['progress']))
        self.assertEquals(1, len(response2.data['progress']))
        self.assertEquals(response1.data, self.get_response(url1).data)
        self.assertNotEqual(response1.data, response2.data)

        # Check: Get invalid challenge id
        url3 = reverse('challenge-participation', args=[1000])
        self.get_response(url3, exp_status_code=status.HTTP_404_NOT_FOUND)

        # Check: Response contains all properties
        self.validate_participation_response(self.get_response(url1))

    def test_delete_participation(self):

        # insert demo data
        clg1, clg2 = self.insert_demo_data()

        # Check: delete existing participation
        url = reverse('challenge-participation', args=[clg1.pk])
        self.delete_response(url)

        part1 = ChallengeParticipation.objects.filter(user=self.test_user, challenge=clg1)[0]
        part2 = ChallengeParticipation.objects.filter(user=self.test_user, challenge=clg2)[0]
        self.assertTrue(part1.mark_deleted)
        self.assertFalse(part2.mark_deleted)

        # Check: Progress is also mark as deleted
        for p in ChallengeProgress.objects.all():
            if p.participation == part1:
                self.assertTrue(p.mark_deleted)
            else:
                self.assertFalse(p.mark_deleted)

        # Check: reactivate participation
        self.post_response(url)

        part1 = ChallengeParticipation.objects.filter(user=self.test_user, challenge=clg1)[0]
        part2 = ChallengeParticipation.objects.filter(user=self.test_user, challenge=clg2)[0]
        self.assertFalse(part1.mark_deleted)
        self.assertFalse(part2.mark_deleted)

        # Check: Progress is also reactivated
        for p in ChallengeProgress.objects.all():
            if p.participation == part1:
                self.assertFalse(p.mark_deleted)

            else:
                self.assertFalse(p.mark_deleted)

    def test_create_progress(self):
        # insert demo data
        clg1, clg2 = self.insert_demo_data()

        part1 = ChallengeParticipation.objects.filter(user=self.test_user, challenge=clg1)[0]
        part2 = ChallengeParticipation.objects.filter(user=self.test_user, challenge=clg2)[0]

        self.assertEquals(3, len(ChallengeProgress.objects.filter(participation=part1)))
        self.assertEquals(1, len(ChallengeProgress.objects.filter(participation=part2)))

        url = reverse('challenge-progress', args=[clg1.pk])
        for i in range(1, 101):
            response = self.post_response(url)
            self.assertEquals(i + 3, len(response.data['progress']))
            if (i + 3) % clg1.duration == 0:
                self.post_response(reverse('challenge-participation', args=[clg1.pk]))

        self.assertEquals(103, len(ChallengeProgress.objects.filter(participation=part1)))
        self.assertEquals(1, len(ChallengeProgress.objects.filter(participation=part2)))

        # Check: Response contains all properties
        self.validate_participation_response(self.post_response(url))
