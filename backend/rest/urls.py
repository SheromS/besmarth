import os

from django.conf import settings
from django.conf.urls import include, url
from django.urls import path
from rest_framework.documentation import include_docs_urls
from rest_framework.routers import DefaultRouter

from .subviews.account_export_view import AccountExportView
from .subviews.account_code_view import AccountCodeView
from .subviews.my_challenge_participations_view import MyChallengeParticipationsView
from .views import DailyTipView, ChallengeViewSet, HealthCheckView, AccountView, TopicViewSet, \
    ChallengeParticipationView, ChallengeProgressView, AccountFriendsView, AccountsView, AccountFriendRequestView, \
    EditAccountFriendRequestView, AccountFriendsEditView, FriendChallengesView, DevTokenView, AchievedRewardView, \
    RewardView, TopicProgressView

commit_sha_short = os.popen("git log -n 1 --pretty=format:'%h'").read()
API_TITLE = 'Besmarth API v-' + commit_sha_short
API_DESCRIPTION = 'A REST API to manage Besmarth data.'

router = DefaultRouter()
router.register(r'challenges', ChallengeViewSet, basename='challenges')
router.register(r'topics', TopicViewSet, basename='topics')


urlpatterns = [
    url(r'^', include(router.urls)),
    url(r'^docs/', include_docs_urls(title=API_TITLE, description=API_DESCRIPTION)),
    url(r'^tips/daily', DailyTipView.as_view(), name='daily-tip'),
    url(r'^account/?$', AccountView.as_view(), name='account'),
    url(r'^account/data$', AccountExportView.as_view(), name='account-data'),

    url(r'^rewards', AchievedRewardView.as_view(), name='rewards'),

    path('challenges/<int:challenge_id>/participation/', ChallengeParticipationView.as_view(),
         name="challenge-participation"),
    path('challenges/<int:challenge_id>/participation/progress/', ChallengeProgressView.as_view(),
         name="challenge-progress"),
    url(r'^achievedReward', AchievedRewardView.as_view(), name='achievedReward'),
    url(r'^account/?$', AccountView.as_view(), name='account'),

    path('reward/<int:reward_id>', RewardView.as_view(), name="reward"),

    path('challenges/<int:challenge_id>/participation/', ChallengeParticipationView.as_view(), name="challenge-participation"),
    path('challenges/<int:challenge_id>/participation/progress/', ChallengeProgressView.as_view(), name="challenge-progress"),

    path('accounts/', AccountsView.as_view(), name='accounts'),
    path('account/friends', AccountFriendsView.as_view(), name='account-friends'),
    path('account/friends/<int:friend_id>/challenges', FriendChallengesView.as_view(), name="account-friend-challenges"),
    path('account/friends/<int:account_id>', AccountFriendsEditView.as_view(), name='account-friends-edit'),

    path('challenges/<int:challenge_id>/participation/', ChallengeParticipationView.as_view(), name="challenge-participation"),
    path('challenges/<int:challenge_id>/participation/progress/', ChallengeProgressView.as_view(), name="challenge-progress"),
    path('account/friends/<int:friend_id>/challenges', FriendChallengesView.as_view(), name="friends-challenges"),
    path('account/friendrequest', AccountFriendRequestView.as_view(), name='account-friend-request'),
    path('account/friendrequest/<int:account_id>', EditAccountFriendRequestView.as_view(),
         name='account-friend-request-edit'),

    path('account/participations', MyChallengeParticipationsView.as_view(), name='my-challenges'),
    path('topics/<int:topic_id>/progress', TopicProgressView.as_view(), name='topic-progress'),
    path('account/code.png', AccountCodeView.as_view(), name='account-code'),

    path('health/<slug:token>', HealthCheckView.as_view(), name='health-check'),
]

# THIS ENDPOINT MUST ONLY EXIST IN DEVELOPMENT MODE
# The client will be provided with a valid account token.
# Even though it shouldn't happen; if the endpoint gets
# published to production, the client should not be able
# do any damage. The corresponding account is created
# separately with no further admin/staff permissions.
if settings.ENVIRONMENT == 'development':
    urlpatterns.append(url(r'account/devtoken/?', DevTokenView.as_view(), name='dev-token'))
