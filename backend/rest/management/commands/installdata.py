from django.core.management.base import BaseCommand
from rest.models import TopicLevel
from django.conf import settings
from shutil import copy2
import os
import rest
from pathlib import Path

# Enhance this command to install more data, besides topic levels.
class Command(BaseCommand):
    help = """Installs default data in database/filesystem (e.g. topic levels).
              Run this command initially when you setup the application in a new environment."""

    def handle(self, *args, **options):
        if TopicLevel.objects.count() > 0:
            result = self.boolean_input("There are existing topic levels in database. Override them? (y/n)")
            if not result:
                self.stdout.write("Cancelled installation.")
                return
            TopicLevel.objects.all().delete()

        self.stdout.write("Installing images...")
        self.install_images()
        self.stdout.write("Installing topic levels...")
        self.install_topic_levels()
        self.stdout.write(self.style.SUCCESS("Successfully installed data"))

    def install_images(self):
        dest = os.path.join(settings.MEDIA_ROOT, "images", "topic-levels")
        # os.path... delivers the absolute path to the rest app/module
        src = os.path.join(os.path.dirname(rest.__file__), "data", "topic-levels")
        # create dest folder if not exists
        Path(dest).mkdir(parents=True, exist_ok=True)
        for i in range(1, 9):
            copy2(os.path.join(src, f"level0{i}.png"), dest)

    def install_topic_levels(self):
        tl1 = TopicLevel(name="Klimaneuling", limit=1, icon="images/topic-levels/level01.png")
        tl2 = TopicLevel(name="Kimasupporter", limit=5, icon="images/topic-levels/level02.png")
        tl3 = TopicLevel(name="Stromsparer", limit=15, icon="images/topic-levels/level03.png")
        tl4 = TopicLevel(name="ÖV-Fahrer", limit=30, icon="images/topic-levels/level04.png")
        tl5 = TopicLevel(name="Recycler", limit=60, icon="images/topic-levels/level05.png")
        tl6 = TopicLevel(name="Bäumepflanzer", limit=100, icon="images/topic-levels/level06.png")
        tl7 = TopicLevel(name="Klimabewegender", limit=150, icon="images/topic-levels/level07.png")
        tl8 = TopicLevel(name="Klimameister", limit=-1, icon="images/topic-levels/level08.png")
        tl1.save()
        tl2.save()
        tl3.save()
        tl4.save()
        tl5.save()
        tl6.save()
        tl7.save()
        tl8.save()

    def boolean_input(self, question, default=None):
        # code from https://stackoverflow.com/questions/39256578/add-confirmation-step-to-custom-django-management-manage-py-command
        result = input("%s " % question)
        if not result and default is not None:
            return default
        while len(result) < 1 or result[0].lower() not in "yn":
            result = input("Please answer yes or no: ")
        return result[0].lower() == "y"
