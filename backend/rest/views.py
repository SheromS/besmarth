from datetime import date
from random import randint
from uuid import uuid1
import os

from django.conf import settings
from django.conf.locale import id
from django.http import HttpResponse
from django.utils.translation import gettext as _
from django.db.models import Q
from rest_framework import status, authentication
from rest_framework import viewsets, generics
from rest_framework.authtoken.models import Token
from rest_framework.authtoken.serializers import AuthTokenSerializer
from rest_framework.decorators import action
from rest_framework.exceptions import ValidationError
from rest_framework.filters import BaseFilterBackend
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from rest_framework.status import HTTP_200_OK
from rest_framework.views import APIView
import coreapi, coreschema
from django.contrib.auth.hashers import make_password

from .models import Tip, TipSchedule, Challenge, Account, Topic, ChallengeParticipation, ChallengeProgress, \
    AchievedReward, Reward, FriendRequest, Friendship, TopicLevel
from .permissions import AccountViewPermission
from .serializers import TipSerializer, ChallengeSerializer, AccountSerializer, CreateAccountSerializer, \
    TopicSerializer, ChallengeParticipationSerializer, AchievedRewardSerializer, RewardSerializer, \
    FriendshipRequestSerializer


def get_friends(user):
    """
    Query all friends of a user by checking
    the receiver and the sender foreign key
    on the Friendship model
    """
    friends = []
    friendships1 = Friendship.objects.filter(receiver=user)
    for friendship in friendships1:
        friends.append(friendship.sender)
    friendships2 = Friendship.objects.filter(sender=user)
    for friendship in friendships2:
        friends.append(friendship.receiver)
    return friends


def get_friendship(user1, user2):
    """
    Get a friendship between two users/accounts
    :param user1: First participant of friendship
    :param user2: Second participant of friendship
    :return: The friendship model or None if friendship does not exists.
    """
    try:
        friendship = FriendRequest.objects.get(sender=user1.id, receiver=user2.id)
        return friendship
    except FriendRequest.DoesNotExist:
        try:
            friendship = FriendRequest.objects.get(sender=user2.id, receiver=user1.id)
            return friendship
        except FriendRequest.DoesNotExist:
            return None


class ChallengeViewSet(viewsets.ReadOnlyModelViewSet):
    """
    Endpoint for challenges
    """
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    queryset = Challenge.objects.all()
    serializer_class = ChallengeSerializer

    # TODO msi instead of url_path use drf-nested-routers mentioned here:
    # https://stackoverflow.com/questions/50425262/django-rest-framework-pass-extra-parameter-to-actions

    @action(detail=False,
            methods=['GET'],
            name="Get Challenges by Topic",
            url_path="topic/(?P<topic_id>\d+)")
    def topic(self, request, *args, **kwargs):
        challenges = Challenge.objects.filter(topic=kwargs['topic_id'])

        if challenges.count == 0:
            return HttpResponse(status=status.HTTP_404_NOT_FOUND)

        serializer = ChallengeSerializer(challenges, many=True, context={'request': request})
        return Response(serializer.data, status=status.HTTP_200_OK)


# Create your views here.
class DailyTipView(generics.RetrieveAPIView):
    """
    Returns the daily tip
    """
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        """ Returns the daily tip"""
        all_tips = Tip.objects.all()
        count = all_tips.count()
        if count < 1:
            return HttpResponse(status=status.HTTP_404_NOT_FOUND)

        try:
            tip = TipSchedule.objects.get(scheduled_at=date.today()).tip
        except TipSchedule.DoesNotExist:
            random = randint(0, count - 1)
            tip = all_tips[random]
        serializer = TipSerializer(tip, context={'request': request})
        return Response(serializer.data)


class AccountView(generics.CreateAPIView, generics.UpdateAPIView, generics.RetrieveAPIView, generics.GenericAPIView):
    """
    get:
        Returns the own account data
    put:
        Updates own account data
    post:
        Login into existing account or create a new account
    """
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (AccountViewPermission,)
    serializer_class = AccountSerializer

    # Get account data
    def get(self, request):
        return Response(AccountSerializer(instance=request.user).data)

    # Create a new account or log into existing account.
    def post(self, request):
        serializer = CreateAccountSerializer(data=request.data)

        serializer.is_valid(raise_exception=True)

        if serializer.validated_data['pseudonymous']:
            # Create new account with now real data
            account = Account(pseudonymous=True, username=uuid1())
            account.save()
            token, created = Token.objects.get_or_create(user=account)
            return Response({
                'token': token.key,
                'user_id': account.pk,
                'username': account.username,
                'pseudonymous': True
            })
        else:
            # Taken from ObtainAuthToken.py
            serializer = AuthTokenSerializer(data=request.data, context={'request': request})
            serializer.is_valid(raise_exception=True)
            account = serializer.validated_data['user']

            if account and not account.pseudonymous:
                token, created = Token.objects.get_or_create(user=account)
                return Response({
                    'token': token.key,
                    'user_id': account.pk,
                    'email': account.email,
                    'username': account.username,
                    'first_name': account.first_name,
                    'pseudonymous': account.pseudonymous
                })
            else:
                raise ValidationError(_('Anonymous user are not allowed to login'), code='invalid')

    # Edit account information
    def patch(self, request):
        serializer = AccountSerializer(instance=request.user, data=request.data, partial=True)
        # print({'email': '', 'username': '', 'first_name': '', 'pseudonymous': True})
        print(request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_200_OK)

    # Delete account
    def delete(self, request):
        # remove personal data
        request.user.username = uuid1()
        request.user.email = ''
        request.user.first_name = ''
        request.user.pseudonymous = True
        request.user.is_active = False
        request.user.password = ''
        request.user.save()

        return Response(AccountSerializer(instance=request.user).data)

class TopicViewSet(viewsets.ReadOnlyModelViewSet):
    """
    API endpoint that allows Topics to be viewed or edited.
    """
    queryset = Topic.objects.all()
    serializer_class = TopicSerializer
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (IsAuthenticated,)


class ChallengeParticipationView(generics.CreateAPIView, generics.DestroyAPIView, generics.RetrieveAPIView):
    serializer_class = ChallengeParticipationSerializer
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get(self, request, challenge_id):
        try:
            participation = ChallengeParticipation.objects.get(user=request.user, challenge=challenge_id)

            serializer = ChallengeParticipationSerializer(instance=participation, context={'request': request})
            return Response(serializer.data)
        except ChallengeParticipation.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

    def patch(self, request, challenge_id):
        try:
            participation = ChallengeParticipation.objects.get(user=request.user, challenge=challenge_id)
        except ChallengeParticipation.DoesNotExist:
            return Response("participations does not exists", status=status.HTTP_404_NOT_FOUND)
        serializer = ChallengeParticipationSerializer(instance=participation, data=request.data, partial=True, context={'request': request})
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_200_OK)

    def post(self, request, challenge_id):
        try:
            challenge = Challenge.objects.get(pk=challenge_id)
        except Challenge.DoesNotExist:
            return Response("invalid challenge", status=status.HTTP_404_NOT_FOUND)

        participations = ChallengeParticipation.objects.filter(user=request.user, challenge=challenge_id)
        if participations.count() == 1:  # count == 1 | 0 : check if participation is present
            participation = participations[0]

            if participation.mark_deleted == False:  # Check if deleted or not
                # Cannot post multiple times if not mark_deleted
                return Response(status=status.HTTP_400_BAD_REQUEST)

            else:
                # participation existing and not deleted --> Restore progress
                participation.mark_deleted = False
                ChallengeProgress.objects.filter(participation=participation).update(mark_deleted=False)

        else:
            # no participation is existing
            participation = ChallengeParticipation(user=request.user, challenge=challenge, mark_deleted=False)

        participation.save()

        serializer = ChallengeParticipationSerializer(instance=participation, context={'request': request})
        return Response(serializer.data, status=status.HTTP_201_CREATED)

    def delete(self, request, challenge_id):
        try:
            participation = ChallengeParticipation.objects.get(user=request.user, challenge=challenge_id)
            ChallengeProgress.objects.filter(participation=participation).update(mark_deleted=True)
        except ChallengeParticipation.DoesNotExists:
            return Response(status=status.HTTP_400_BAD_REQUEST)

        participation.mark_deleted = True
        participation.save()

        return Response(status=status.HTTP_200_OK)


class ChallengeProgressView(generics.CreateAPIView, ):
    serializer_class = ChallengeParticipationSerializer
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def post(self, request, challenge_id):
        query_set_part = ChallengeParticipation.objects.filter(user=request.user, challenge=challenge_id)

        if query_set_part.count() == 0 or query_set_part[0].mark_deleted:  # count > 1 not possible
            return Response(status=status.HTTP_404_NOT_FOUND)

        # TODO msi, endless insert should not be possible
        participation = query_set_part[0]

        if(len(participation.progress.all()) % participation.challenge.duration == participation.challenge.duration -1): #unfollow challenge
            participation.mark_deleted = True
            participation.save()

        progress = ChallengeProgress(mark_deleted=False, participation=participation)
        progress.save()

        serializer = ChallengeParticipationSerializer(instance=participation, context={'request': request})
        return Response(data=serializer.data, status=status.HTTP_201_CREATED)

class FriendChallengesView(generics.ListAPIView):
    serializer_class = ChallengeParticipationSerializer
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get(self, request, friend_id):
        friend = Account.objects.get(pk=friend_id)
        if 1 != Friendship.objects.filter(
                Q(receiver=request.user, sender=friend) | Q(receiver=friend, sender=request.user)).count():
            return Response(status=status.HTTP_403_FORBIDDEN)

class FriendChallengesView(generics.ListAPIView):
    serializer_class = ChallengeParticipationSerializer
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get(self, request, friend_id):
        friend = Account.objects.get(pk=friend_id)
        if 1 != Friendship.objects.filter(
                Q(receiver=request.user, sender=friend) | Q(receiver=friend, sender=request.user)).count():
            return Response(status=status.HTTP_403_FORBIDDEN)

        participations = ChallengeParticipation.objects.filter(user=friend, shared=True, mark_deleted=False)
        serializer = ChallengeParticipationSerializer(participations, many=True, context={'request': request})
        return Response(serializer.data)

class AccountFriendsView(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        """
        Get friends of authenticated user.
        Return Code: HTTP 200.
        Return Data: a list of accounts.
        """
        friends = get_friends(request.user)

        serializer = AccountSerializer(friends, many=True, context={'request': request})
        return Response(serializer.data, status=status.HTTP_200_OK)


class AccountFriendsEditView(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def is_involved(self, friendship, user_id):
        """
        Checks whether a user id is involved in a friendship
        :return: true if involved, otherwise false
        """
        return friendship.sender.id != user_id and friendship.receiver.id != user_id

    def delete(self, request, account_id):
        """
        Delete a friendship. Parameter: {account_id} the ID of the friend which should be deleted.
        Error Code: HTTP 404.
        Return Code: HTTP 200.
        Return Data: a list of all friends (which is a list of accounts).
        """
        try:
            friendship = Friendship.objects.get(sender=account_id, receiver=request.user.id)
        except Friendship.DoesNotExist:
            try:
                friendship = Friendship.objects.get(sender=request.user.id, receiver=account_id)
            except Friendship.DoesNotExist:
                return Response("friendship does not exist.", status=status.HTTP_404_NOT_FOUND)

        if self.is_involved(friendship, request.user.id):
            return Response("friendship does not exist.", status=status.HTTP_404_NOT_FOUND)

        friendship.delete()
        friends = get_friends(request.user)

        serializer = AccountSerializer(friends, many=True, context={'request': request})
        return Response(serializer.data, status=status.HTTP_200_OK)


# https://stackoverflow.com/a/41804036
class DirectionUrlParameter(BaseFilterBackend):
    def get_schema_fields(self, view):
        return [coreapi.Field(
            name='direction',
            location='query',
            required=False,
            type='string',
            schema=coreschema.String(
                description='Filter the friend requests by incoming/outgoing. Possible values: in | out. Default: in.'),
        )]


class AccountFriendRequestView(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    filter_backends = (DirectionUrlParameter,)

    PARAM_IN = 'in'
    PARAM_OUT = 'out'

    def get(self, request):
        """
        Get friend requests of authenticated user.
        Friend requests can be filtered using the `direction=in` or `direction=out` parameter in the URL.
        Return Data: a list of friend requests.
        """
        direction = request.query_params.get('direction', self.PARAM_IN)
        if direction != 'in' and direction != 'out':
            return Response("`{}` is an invalid `direction` URL parameter. Allowed values are: {} | {}".format(
                direction, self.PARAM_IN, self.PARAM_OUT), status=status.HTTP_400_BAD_REQUEST)

        if direction == self.PARAM_IN:
            friend_requests = FriendRequest.objects.filter(receiver=request.user)
        else:
            friend_requests = FriendRequest.objects.filter(sender=request.user)

        serializer = FriendshipRequestSerializer(friend_requests, many=True, context={'request': request})
        return Response(serializer.data, status=status.HTTP_200_OK)

    def post(self, request):
        """
        Create a friend request.
        Expects a json body with a `receiver` key whose value is the unique ID of the receiver account.
        Error Codes: HTTP 400 or HTTP 404.
        Return Code: HTTP 201.
        Return Data: a list of all outgoing friend requests.
        """
        receiver_str = request.data.get('receiver')
        if not receiver_str:
            return Response("no receiver specified", status=status.HTTP_400_BAD_REQUEST)

        try:
            receiver_id = int(receiver_str)
        except ValueError:
            return Response("receiver must be an integer", status=status.HTTP_400_BAD_REQUEST)

        try:
            receiver_account = Account.objects.get(id=receiver_id)
        except Account.DoesNotExist:
            return Response("receiver account does not exist", status=status.HTTP_404_NOT_FOUND)

        if receiver_account.id == request.user.id:
            return Response("you can not add yourself as a friend", status=status.HTTP_400_BAD_REQUEST)

        if FriendRequest.objects.filter(sender=request.user, receiver=receiver_account).exists():
            return Response("there is already a friend request outstanding for this receiver",
                            status=status.HTTP_400_BAD_REQUEST)

        current_friends = get_friends(request.user)
        for friend in current_friends:
            if friend.id == receiver_id:
                return Response("receiver is already a friend",
                                status=status.HTTP_400_BAD_REQUEST)

        friend_request = FriendRequest(sender=request.user, receiver=receiver_account)
        friend_request.save()
        outgoing_friend_requests = FriendRequest.objects.filter(sender=request.user)
        serializer = FriendshipRequestSerializer(outgoing_friend_requests, many=True)
        return Response(serializer.data, status=status.HTTP_201_CREATED)


class EditAccountFriendRequestView(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def put(self, request, account_id):
        """
        Accept a friend request.
        This means a friendship is created and the friend request will be deleted.
        Parameter: {account_id} the ID of the other friendship participant.
        Error Codes: HTTP 404 if account or friend request does not exist.
        Return Code: HTTP 201.
        Return Data: a list of all friends.
        """
        try:
            account = Account.objects.get(pk=account_id)
        except Account.DoesNotExist:
            return Response("account does not exist", status=status.HTTP_404_NOT_FOUND)

        friendrequest = get_friendship(request.user, account)
        if friendrequest is None:
            return Response("friend request does not exist", status=status.HTTP_404_NOT_FOUND)

        if friendrequest.receiver.id != request.user.id:
            # We also return a 404 if the client tries to accept a friend request
            # that isn't addressed to him. An alternative would be to return
            # 403 Forbidden but this would give the client information about the
            # existence of other friend requests thus could lead to vulnerability.
            return Response("friend request does not exist", status=status.HTTP_404_NOT_FOUND)

        friendship = Friendship(sender=friendrequest.sender, receiver=friendrequest.receiver)
        friendship.save()

        friendrequest.delete()
        friends = get_friends(request.user)

        serializer = AccountSerializer(friends, many=True, context={'request': request})
        return Response(serializer.data, status=status.HTTP_201_CREATED)

    def delete(self, request, account_id):
        """
        Delete a friend request.
        Parameter: {account_id} the ID of the other friendship participant.
        Error Codes: HTTP 404 if friend request does not exist.
        Return Code: HTTP 200.
        Return Data: a list of all outgoing friend requests.
        """
        try:
            account = Account.objects.get(pk=account_id)
        except Account.DoesNotExist:
            return Response("account does not exist", status=status.HTTP_404_NOT_FOUND)

        friendrequest = get_friendship(request.user, account)
        if friendrequest is None:
            return Response("friend request does not exist", status=status.HTTP_404_NOT_FOUND)

        if not (friendrequest.receiver.id == request.user.id or friendrequest.sender.id == request.user.id):
            # We also return a 404 if the client tries to accept a friend request
            # that isn't addressed to or from him. An alternative would be to return
            # 403 Forbidden but this would give the client information about the
            # existence of other friend requests thus could lead to vulnerability.
            return Response("friend request does not exist", status=status.HTTP_404_NOT_FOUND)

        friendrequest.delete()
        outgoing_friend_requests = FriendRequest.objects.filter(sender=request.user)
        serializer = FriendshipRequestSerializer(outgoing_friend_requests, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


# https://stackoverflow.com/a/41804036
class QueryUrlParameter(BaseFilterBackend):
    def get_schema_fields(self, view):
        return [coreapi.Field(
            name='query',
            location='query',
            required=False,
            type='string',
            schema=coreschema.String(description='(optional) The query to be used for search.'),
        )]


class AccountsView(generics.RetrieveAPIView, ):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    filter_backends = (QueryUrlParameter,)

    def get(self, request):
        """
        Search other accounts based on the `query` parameter in the URL.
        Search is done case insensitive.
        Returns a list of accounts matching the query.
        """
        # https://www.django-rest-framework.org/api-guide/requests/#query_params
        query = request.query_params.get('query', '')
        if query:
            # https://docs.djangoproject.com/en/2.2/ref/models/querysets/#icontains
            accounts = Account.objects.filter(username__icontains=query)
        else:
            accounts = Account.objects.all()

        serializer = AccountSerializer(accounts, many=True, context={'request': request})
        return Response(serializer.data, status=status.HTTP_200_OK)


class RewardView(generics.RetrieveAPIView):
    serializer_class = RewardSerializer
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get(self, request, reward_id):
        query_set = Reward.objects.get(pk=reward_id)

        serializer = RewardSerializer(query_set, context={'request': request})
        return Response(serializer.data, status=HTTP_200_OK)


class AchievedRewardView(generics.RetrieveAPIView):
    serializer_class = AchievedRewardSerializer
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get(self, request, *args, **kwargs):
        query_set = AchievedReward.objects.filter(user=request.user)

        if query_set.count() == 0:  # count > 1 not possible
            return Response(status=status.HTTP_404_NOT_FOUND)

        serializer = AchievedRewardSerializer(query_set, many=True, context={'request': request})
        return Response(serializer.data, status=HTTP_200_OK)


def create_tester_account(username):
    testuser = Account()
    testuser.username = username
    testuser.first_name = "John"
    testuser.last_name = "Doe"
    testuser.email = "john@doe.com"
    testuser.is_active = True
    testuser.password = make_password("12345")
    testuser.save()
    return testuser


class DevTokenView(APIView):

    def get(self, request):
        if settings.ENVIRONMENT != 'development':
            return Response(status=status.HTTP_404_NOT_FOUND)

        username = "johnny"
        try:
            testuser = Account.objects.get(username=username)
        except Account.DoesNotExist:
            testuser = create_tester_account(username)

        token, created = Token.objects.get_or_create(user=testuser)

        return Response(token.key, status=status.HTTP_200_OK)

def lazy_init_topic_level():
    if TopicLevel.objects.count() > 0:
        return
    beginner = TopicLevel(name="Anfänger", limit=10)
    beginner.save()
    intermediate = TopicLevel(name="Fortgeschrittener", limit=100)
    intermediate.save()
    master = TopicLevel(name="Meister", limit=1000)
    master.save()
    max = TopicLevel(name="Max Level", limit=-1)
    max.save()


class TopicProgressView(APIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get(self, request, topic_id):
        """
        Get user progress in a topic (refers to progress of authenticated user.)
        Parameter: {topic_id} the id of the topic.
        Error Codes: HTTP 404 if topic does not exist.
        Return Code: HTTP 200.
        Return Data: A json object containing the following data:
                       { level: int,
                         score: int,
                         scoreNextLevel: int,
                         scorePrevLevel: int,
                         levelDescription: string,
                         imageUrl: string
                       }
        """
        try:
            Topic.objects.get(pk=topic_id)
        except Topic.DoesNotExist:
            return Response("topic does not exist", status=status.HTTP_404_NOT_FOUND)

        lazy_init_topic_level()
        user_id = request.user.id

        # This query looks for the progress of all challenges that are part of a specific topic
        # and in which the user participates in.
        # Progress is represented by the number of times a given challenge has been completed.
        # It is calculated using an integer division that means only completed challenges will
        # be reflected in the progress.
        # The query result contains: challenge_id, challenge_title, challenge_difficulty, progress
        progress_qry = f"""
                SELECT c.id, c.title, c.difficulty, count(cprogress.id)/c.duration AS 'progress'
                FROM rest_challengeprogress cprogress
                JOIN rest_challengeparticipation cparticipation ON cprogress.participation_id = cparticipation.id 
                JOIN rest_challenge c on cparticipation.challenge_id = c.id
                JOIN rest_topic t on c.topic_id = t.id
                WHERE cparticipation.user_id={user_id}
                AND t.id={topic_id}
                GROUP BY c.id
                """

        # difficulty factors used as a multiplier to calculate absolute score
        d_factors = {
            Challenge.DIFFICULTY_CHOICES[0][0]: 1,
            Challenge.DIFFICULTY_CHOICES[1][0]: 2,
            Challenge.DIFFICULTY_CHOICES[2][0]: 3,
        }

        # calculate abosolute user score in topic
        score = 0
        challenge_progresses = ChallengeProgress.objects.raw(progress_qry)
        for cp in challenge_progresses:
            score += cp.progress * d_factors[cp.difficulty]

        # determine current and next level

        # max_level is defined as the level with a negative limit
        max_level = TopicLevel.objects.filter(limit__lt=0).get()
        max_level_icon_url = None if (not max_level.icon) else max_level.icon.url
        # the count will at least return 1 because it is a constraint,
        # that the database contains an entry with a negative limit
        # which represents the max level
        level = TopicLevel.objects.filter(limit__lte=score).count()
        # all levels without the max_level
        levels = TopicLevel.objects.order_by('limit').filter(limit__gte=0).all()
        # check if max_level reached
        if level > len(levels):
            score_prev_level = levels.last().limit
            score_next_level = score
            level_description = max_level.name
            image_url = request.build_absolute_uri(max_level_icon_url)
        else:
            # the first level has no predecessor
            score_prev_level = 0 if level is 1 else levels[level-2].limit
            level_description = levels[level-1].name
            image_url = None if not levels[level-1].icon else request.build_absolute_uri(levels[level-1].icon.url)
            score_next_level = levels[level-1].limit

        response = {
            "score": score,
            "scoreNextLevel": score_next_level,
            "scorePrevLevel": score_prev_level,
            "level": level,
            "levelDescription": level_description,
            "imageUrl": image_url
        }

        return Response(response, status=status.HTTP_200_OK)


class HealthCheckView(APIView):

    def get(self, request, token):
        """
        Make a server health check. Meant to be called from ci/cd after a deployment.

        Error Codes: HTTP 500 if health check failed.
                     HTTP 403 if token invalid
        Success Code: HTTP 200.
        Return Data: A key value json object listing several health indicators
                     and whether they are OK (0=NOK everything else OK). The
                     server will return as soon as a check fails, such that
                     all succeeding checks will be listed as fails too, that
                     has to be interpreted as 'not checked'.
        """
        # random but hard coded token to prevent making the endpoint public
        expected_token = "fai984oier39qerpf437ianzi-fdqsiur2ck"
        if token != expected_token:
            return Response("unauthorized", status=status.HTTP_403_FORBIDDEN)

        response = {
            "db_connectivity": False
        }

        try:
            # 1. CHECK DB CONNECTIVITY
            # all() returns a queryset without evaluating it (i.e. no db connect)
            # that's why we wrap it in list() which will force evaluation
            list(Account.objects.all())
            response["db_connectivity"] = True
            return Response(response, status=status.HTTP_200_OK)
        except Exception as e:
            return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)
