from django.contrib.auth.hashers import make_password
from rest_framework import serializers

from .models import Tip, Challenge, Account, Topic, ChallengeParticipation, ChallengeProgress, FriendRequest, Reward, \
    AchievedReward, Friendship


class CreateAccountSerializer(serializers.Serializer):
    """ Serializer for POST /account  most fields from account are not needed, therefore this is not a ModelSerializer"""
    username = serializers.CharField(required=False)
    password = serializers.CharField(required=False)
    pseudonymous = serializers.BooleanField(default=False, required=False)

    def validate(self, attrs):
        pseudonymous = attrs.get('pseudonymous')
        username = attrs.get('username')
        password = attrs.get('password')

        if pseudonymous or (username and password):
            pass
        else:
            raise serializers.ValidationError('Must include either "pseudonymous" or "username" and "password".')
        return attrs

    def create(self, validated_data):
        pass

    def update(self, instance, validated_data):
        pass


class AccountSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Account
        fields = ['id', 'username', 'email', 'password', 'first_name', 'pseudonymous']

    @staticmethod
    def validate_password(value: str) -> str:
        return make_password(value)

    def to_representation(self, obj):
        ret = super(AccountSerializer, self).to_representation(obj)
        # do not output password
        ret.pop('password')
        return ret


class TipSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Tip
        fields = ['id', 'title', 'description', 'image']


class TopicSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Topic
        fields = ['id', 'internal_id', 'topic_name', 'topic_description', 'image_level1', 'image_level2', 'image_level3',
                  'image_level4', 'image_level5']


class ChallengeSerializer(serializers.HyperlinkedModelSerializer):
    topic = serializers.SlugRelatedField(
        many=False,
        read_only=True,
        slug_field='topic_name'
    )

    class Meta:
        model = Challenge
        fields = ['id', 'title', 'description', 'icon', 'image',
                  'duration', 'color', 'difficulty', 'periodicity', 'topic',
                  'category']

    def to_representation(self, obj):
        ret = super(ChallengeSerializer, self).to_representation(obj)
        ret['image'] = self.context['request'].build_absolute_uri(ret['image'])
        ret['icon'] = self.context['request'].build_absolute_uri(ret['icon'])
        return ret


class ChallengeProgressSerializer(serializers.HyperlinkedModelSerializer):
    participation = serializers.PrimaryKeyRelatedField(many=False, read_only=True)

    class Meta:
        model = ChallengeProgress
        fields = ['id', 'create_time', 'participation', 'mark_deleted']


class ChallengeParticipationSerializer(serializers.HyperlinkedModelSerializer):
    challenge = ChallengeSerializer(many=False, read_only=True)
    progress = ChallengeProgressSerializer(many=True)
    progress_loggable = serializers.ReadOnlyField()

    class Meta:
        model = ChallengeParticipation
        fields = ['id', 'joined_time', 'challenge', 'progress', 'shared', 'mark_deleted', 'progress_loggable']


class FriendshipRequestSerializer(serializers.ModelSerializer):
    receiver = AccountSerializer()
    sender = AccountSerializer()

    class Meta:
        model = FriendRequest
        fields = ['id', 'receiver', 'sender', 'date']


class RewardSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Reward
        fields = ['id', 'type', 'color', 'points', 'condition']


class AchievedRewardSerializer(serializers.HyperlinkedModelSerializer):
    reward = RewardSerializer()

    class Meta:
        model = AchievedReward
        fields = ['reward']


class AccountExportSerializer(serializers.ModelSerializer):
    class Meta:
        model = Account
        fields = ['id', 'username', 'email', 'first_name', 'last_name', 'pseudonymous', 'is_active', 'date_joined',
                  'last_login']


class MinimalAccountSerializer(serializers.ModelSerializer):
    class Meta:
        model = Account
        fields = ['id', 'username', 'first_name']


class MinimalFriendshipSerializer(serializers.ModelSerializer):
    receiver = MinimalAccountSerializer()
    sender = MinimalAccountSerializer()

    class Meta:
        model = Friendship
        fields = ['receiver', 'sender', 'date']
