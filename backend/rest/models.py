import datetime
from datetime import timedelta

from django.contrib.auth.models import User, AbstractUser
from django.db import models
from django.utils import timezone
from django.utils.datetime_safe import datetime
from django.utils.functional import cached_property


class Account(AbstractUser):
    class Meta:
        db_table = 'auth_user'

    pseudonymous = models.BooleanField(default=False, blank=False, null=False)
    password = models.CharField('password', max_length=128, blank=True)

    def __str__(self):
        return self.username


class Friendship(models.Model):
    # related_name has to be specified because auto generation fails when having to foreign keys
    # to the same entity, in this case account
    # for explanation of related_name see:
    # https://stackoverflow.com/questions/2642613/what-is-related-name-used-for-in-django
    sender = models.ForeignKey(Account, on_delete=models.CASCADE, related_name="friendship_sender_set")
    receiver = models.ForeignKey(Account, on_delete=models.CASCADE, related_name="friendship_receiver_set")
    date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.sender.username + " friend with " + self.receiver.username


class FriendRequest(models.Model):
    sender = models.ForeignKey(Account, on_delete=models.CASCADE, related_name="friend_request_sender_set")
    receiver = models.ForeignKey(Account, on_delete=models.CASCADE, related_name="friend_request_receiver_set")
    date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.sender.username + " sent friend request to " + self.receiver.username


class Tip(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    title = models.CharField(max_length=100)
    description = models.TextField()
    image = models.ImageField(upload_to='images/tips')

    def __str__(self):
        return self.title

    class Meta:
        ordering = ['created']


class TipSchedule(models.Model):
    tip = models.ForeignKey(Tip, on_delete=models.CASCADE)
    scheduled_at = models.DateField()

    def __str__(self):
        return self.scheduled_at

    class Meta:
        ordering = ['scheduled_at']


class Topic(models.Model):
    _img_path = 'images/topics/'

    SUPPORTED_TOPICS = [
        (0, "Forest"),
        (1, "Waste"),
        (2, "Water / Ocean"),
        (3, "Food"),
        (4, "Travelling"),
        (5, "Shopping"),
        (6, "Energy")
    ]

    # The internal id is used to specify the topic
    internal_id = models.IntegerField(unique=True, choices=SUPPORTED_TOPICS);
    topic_name = models.CharField(max_length=25,                      help_text="Name of Topic")
    topic_description = models.CharField(max_length=256,  blank=True, help_text="A short, wordy description of the topic.")
    image_level1 = models.ImageField(upload_to=_img_path, blank=True)
    image_level2 = models.ImageField(upload_to=_img_path, blank=True)
    image_level3 = models.ImageField(upload_to=_img_path, blank=True)
    image_level4 = models.ImageField(upload_to=_img_path, blank=True)
    image_level5 = models.ImageField(upload_to=_img_path, blank=True)

    def __str__(self):
        return " {} - {}".format(self.SUPPORTED_TOPICS[self.internal_id][1], self.topic_name)


class Challenge(models.Model):
    class Meta:
        ordering = ['title']

    DIFFICULTY_CHOICES = [
        ("EASY", "Easy"),
        ("ADVANCED", "Advanced"),
        ("HARD", "Hard"),
    ]

    PERIODICITY_CHOICES = [
        ("DAILY", "Daily"),
        ("WEEKLY", "Weekly"),
        ("MONTHLY", "Monthly"),
    ]

    CATEGORY_CHOICES = [
        ("MOBILITY", "Mobilität"),
        ("SHOPPING", "Shopping"),
        ("LIFE", "Leben"),
        ("WORK_EDU", "Arbeiten / Ausbildung"),
    ]

    title = models.CharField(max_length=100)
    description = models.TextField()
    icon = models.ImageField(upload_to='images/challenges', blank=True)
    image = models.ImageField(upload_to='images/challenges', blank=True)
    duration = models.IntegerField(help_text="Duration of the challenge")
    color = models.CharField(max_length=9, help_text="Hex code for color. Format #RRGGBB")
    difficulty = models.CharField(
        max_length=10,
        choices=DIFFICULTY_CHOICES,
        default=DIFFICULTY_CHOICES[0][0]
    )
    periodicity = models.CharField(
        max_length=10,
        choices=PERIODICITY_CHOICES,
        default=PERIODICITY_CHOICES[0][0]
    )
    topic = models.ForeignKey(Topic, on_delete=models.PROTECT)
    category = models.CharField(
        max_length=10,
        choices=CATEGORY_CHOICES,
        default=CATEGORY_CHOICES[0][0])

    def __str__(self):
        return self.title


class ChallengeParticipation(models.Model):
    class Meta:
        unique_together = (('user', 'challenge'),)

    joined_time = models.DateTimeField(default=timezone.now, help_text="Time when the user joined the challenge")
    user = models.ForeignKey(Account, on_delete=models.CASCADE, help_text="Linked User")
    challenge = models.ForeignKey(Challenge, on_delete=models.SET_NULL, null=True, help_text="Linked Challenge")
    mark_deleted = models.BooleanField(default=False, help_text="True if the Record is marked to delete")
    shared = models.BooleanField(default=False, help_text="True if the participation is visible to the user's friends")

    @cached_property
    def progress_loggable(self):
        """ Calculates wheter or not progress can logged """
        today = datetime.now().date()
        periodicity = self.challenge.periodicity
        if periodicity == 'DAILY':
            today = today - timedelta(days=1)
        elif periodicity == 'WEEKLY':
            today = today - timedelta(weeks=1)
        elif periodicity == 'MONTHLY':
            today = today - timedelta(weeks=4)
        else:
            pass

        try:
            # load latest progress (could not exists, therefore try except)
            last_progress = self.progress.latest('create_time')
        except ChallengeProgress.DoesNotExist:
            pass

        progress_count = self.progress.count()
        # latest progress must be X days/weeks/months ago AND total count must be lower than total duration
        return progress_count == 0 or (
                    last_progress.create_time.date() <= today and progress_count < self.challenge.duration)


class ChallengeProgress(models.Model):
    create_time = models.DateTimeField(default=timezone.now,
                                       help_text="Time when the progress was modified")
    participation = models.ForeignKey(ChallengeParticipation, on_delete=models.CASCADE,
                                      help_text="Linked ChallengeParticipation",
                                      related_name='progress')  # related_name makes progress available on ChallengeParticipation
    mark_deleted = models.BooleanField(default=False, help_text="Returns true if the Record is marked to delete");


class Reward(models.Model):
    TYPE_CHOICES = [
        ("BADGE", "Badge"),
        ("MEDAL", "Medal"),
        ("TROPHY", "Trophy"),
    ]

    type = models.CharField(
        max_length=10,
        choices=TYPE_CHOICES,
        default=TYPE_CHOICES[0]
    )
    color = models.CharField(max_length=9, help_text="Hex code for color. Format #RRGGBB")
    points = models.IntegerField()
    condition = models.IntegerField()


class AchievedReward(models.Model):
    user = models.ForeignKey(Account, on_delete=models.CASCADE)
    reward = models.ForeignKey(Reward, on_delete=models.CASCADE)
    achieved_time = models.DateTimeField(auto_now_add=True, editable=False,
                                         help_text="Time when the user achieved that reward")

    class Meta:
        unique_together = ('user', 'reward')


class TopicLevel(models.Model):
    name = models.CharField(max_length=30, help_text="Level name, e.g 'Rookie'.")
    limit = models.IntegerField(help_text="The max score to which this level goes, "
                                          "exceeding this score will result in moving on to the next level.")
    icon = models.ImageField(upload_to='images/topic-levels', blank=False)
