#!/bin/bash

##################################
# Creates a db dump and executes
# python manage.py afterwards.
###################################

set -e

# the directory in backend container, where the dump should be placed
# make sure this place is mounted as a volume, such that the data
# persists.
BACKUP_DIR="/var/lib/besmarth/backup"
DB_CONTAINER="besmarth_mysql"
BACKEND_CONTAINER="besmarth_backend"
DB_NAME="besmarth"
DB_USER="root"
DB_PASSWORD="besmarth_mysql_pw"
# date used in sql dump file name
printf -v DATE '%(%Y-%m-%d_%H-%M-%S)T' -1

# subsequent -it ensures that return code is reported back to the shell
# only proceed if django check passes (exit code is 1 if check doesn't pass)
docker exec $BACKEND_CONTAINER python manage.py check
# create dump
docker exec $DB_CONTAINER bash -c "mysqldump --user=$DB_USER --password='$DB_PASSWORD' -h 127.0.0.1 $DB_NAME > dump.sql"
# copy dump
docker exec $BACKEND_CONTAINER mkdir -p $BACKUP_DIR
docker cp $DB_CONTAINER:dump.sql /tmp/dump.sql
docker cp /tmp/dump.sql $BACKEND_CONTAINER:$BACKUP_DIR/$DATE.sql
# run migrations
docker exec $BACKEND_CONTAINER python manage.py migrate
