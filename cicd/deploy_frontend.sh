#!/bin/bash

# causes the script to abort for any command that fails
set -e

CI_BUILD_TOKEN="{CI_BUILD_TOKEN}"
FRONTEND_LATEST="{FRONTEND_LATEST}"

docker login -u gitlab-ci-token -p $CI_BUILD_TOKEN registry.gitlab.com
docker pull $FRONTEND_LATEST
docker container stop besmarth_frontend && docker container rm besmarth_frontend || true

docker run -d \
--publish 19000:19000 \
--publish 19001:19001 \
--publish 19002:19002 \
--privileged \
--network besmarth \
--name besmarth_frontend $FRONTEND_LATEST

# The following two commands are in order allow access to the
# Expo Developer Tools which usually only listens to localhost:19002.
# With the iptables entry all *:19002 requests are redirected to
# localhost:19002 (from the container point of view). Prerequisite to
# this approach is to start the container in --privileged mode
# Sources:
# https://unix.stackexchange.com/questions/111433/iptables-redirect-outside-requests-to-127-0-0-1
# https://superuser.com/questions/661772/iptables-redirect-to-localhost
docker exec besmarth_frontend bash -c 'apt-get update && apt-get install --yes iptables && sysctl -w net.ipv4.conf.all.route_localnet=1 && iptables -t nat -I PREROUTING -p tcp --dport 19002 -j DNAT --to 127.0.0.1:19002'
