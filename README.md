# besmarth

[Website](https://besmarth.ch) | [API](https://besmarth.ch/docs) | [Wiki](https://gitlab.com/besmarth/besmarth/-/wikis) | [Contribute](https://gitlab.com/besmarth/besmarth/-/wikis/Contribution-Guideline) | [Dev Env](https://gitlab.com/besmarth/besmarth/-/wikis/Development-Environment)

besmarth is an Android/iOS app where a community comes together, striving to maintain a clean and sustainable planet.

With besmarth, you can participate in a variety of challenges, ranging from easy to expert, and collect rewards for each completed challenge.
Challenges are available in a broad range of categories, such that you can pick just the right one and support what you find most important.
After having completed a bunch of challenges, besmarth provides insights about the impact of your hard work on the environment.

## Contributing

**Interested in contributing?** The [Contribution Guideline](https://gitlab.com/besmarth/besmarth/-/wikis/Contribution-Guideline) is the best place to start.

besmarth originally was a students project at the University of Applied Sciences and Arts in Switzerland.
Fortunately, the source code could be released under the [GNU General Public License](https://www.gnu.org/licenses/gpl-3.0.txt) after the project has ended.
That means you are free to redistribute and/or modify the code, but in particular, it means that **you are welcome to contribute to this project**.

Everybody can contribute, either with code, documentation or simply with ideas/suggestions. Check out the [Contribution Guideline](https://gitlab.com/besmarth/besmarth/-/wikis/Contribution-Guideline) to get started.
(It's a quick read but ensures that you are aware of our development workflow, such that you can contribute in a valuable way.)
We (seven CS students) try to continue the work on this project as well and we would be delighted to see you join us.

## Wiki

Documentation and guides are maintained in the [Wiki](https://gitlab.com/besmarth/besmarth/-/wikis).

## License

Copyright 2019 besmarth (we are a group of 7 people, whose emails are listed below)

nicole.keonigstein@students.fhnw.ch
steve.vogel@students.fhnw.ch
marco.waldmeier@students.fhnw.ch
samuel.keusch@students.fhnw.ch
louisa.reinger@students.fhnw.ch
marc.sieber@students.fhnw.ch
noethiger.mike@gmail.com

besmarth is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3 of the License.

besmarth is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program (COPYING file.) If not, see <https://www.gnu.org/licenses/>.