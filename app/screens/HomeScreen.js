import React from "react";
import {Dimensions, SafeAreaView, StyleSheet, Text, View} from "react-native";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import MapItem from "../components/MapItem";
import {fetchTopics} from "../services/Topics";
import SwiperFlatList from "react-native-swiper-flatlist";
import theme from "../themes/theme";

class HomeScreen extends React.Component {

  componentDidMount() {
    this.reload();
  }

  reload() {
    this.props.fetchTopics();
  }

  render() {
    const topics = this.props.topics;
    if (topics.length > 0) {
      const mapViews = topics.map(topic => {
        return <View style={styles.page} key={topic.internal_id}>
          <MapItem topic={topic} key={topic.internal_id}/>
        </View>;
      });

      return (
        <SafeAreaView style={styles.container}>
          <SwiperFlatList style={styles.container}
                          index={0}
                          showPagination
                          autoplay
                          autoplayLoop
                          autoplayDelay={5}
                          paginationActiveColor={theme.colors.primary}>
            {mapViews}
          </SwiperFlatList>
        </SafeAreaView>
      );
    } else {
      return (
        <View style={styles.container}>
          <Text>Es sind keine Topics vorhanden.</Text>
        </View>
      );
    }
  }
}

const {width, height} = Dimensions.get("window");
const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexWrap: "wrap",
    flexDirection: "row",
  },
  page: {
    height: height,
    width: width
  }
});

const mapStateToProps = state => {
  return {
    topics: Object.values(state.topics.topics),
    refreshing: state.topics.refreshing,
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators({
    fetchTopics,
  }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen);