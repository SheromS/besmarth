import React from "react";
import {FlatList, RefreshControl, ScrollView, StyleSheet, View} from "react-native";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import {Button, Colors, Headline, IconButton, Paragraph, Searchbar, Text, TouchableRipple} from "react-native-paper";
import Navigation from "../../services/Navigation";
import {showToast} from "../../services/Toast";

import SegmentedControlTab from "react-native-segmented-control-tab";
import {
  acceptFriendRequest,
  createFriendRequest,
  deleteFriend,
  deleteFriendRequest,
  fetchFriends,
  fetchIncomingFriendRequests,
  fetchOutgoingFriendRequests,
  searchUser
} from "../../services/FriendService";
import debounce from "debounce";
import {notNull} from "../../services/utils";

// Index Vor Tabbed View
const SEARCH_VIEW = 0;
const REQUEST_INCOMING_VIEW = 1;
const REQUEST_OUTGOING_VIEW = 2;

// Values for User Type Map
const UT_FRIEND = "friend";
const UT_FRIEND_REQUEST_SENT = "request_sent";
const UT_FRIEND_REQUEST_INCOMING = "request_incoming";

class FriendsScreen extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      selectedIndex: SEARCH_VIEW,
      friendSearch: "",
      friends: [],
      strangers: []
    };
  }

  // Setting the index of SegmentedControlTab
  handleIndexChange = index => {
    this.setState({
      ...this.state,
      selectedIndex: index
    });
  };


  /**
   * Redirects the user to SignIn screen if he is not signed in
   */
  componentDidMount() {
    if (this.props.signedIn) {
      this.props.navigation.setOptions({
        headerRight: () => (
          <IconButton
            onPress={() => Navigation.navigate("FriendsTab", "AccountCode")}
            accessibilityLabel="Eigenen QR-Code anzeigen"
            icon="qrcode"
            testID="toggle-qrcode"
            color="black"
            size={20}/>
        )
      });
    }

    this.navFocusListenerSub = this.props.navigation.addListener("focus", this.onScreenFocus.bind(this));
  }

  componentWillUnmount() {
    this.navFocusListenerSub();
  }

  /**
   * Called when the user focuses this screen
   */
  async onScreenFocus() {
    if (notNull(this.props?.route?.params?.scannedCode)) {
      this.onSearch(this.props?.route?.params?.scannedCode);
      this.props.navigation.setParams({scannedCode: null});
      // do not reload, this is already done by onSearch
    } else {
      this.reload().then();
    }
  }

  async reload() {
    if (!this.props.signedIn) {
      // do not load anything if not signed in
      return;
    }
    this.setState({
      ...this.state,
      loading: true,
    });

    this.searchUsersImmediately(this.state.friendSearch);
  }

  openScanner() {
    Navigation.navigate("FriendsTab", "ScanFriendCode");
  }

  async onSearch(value) {
    this.setState({
      ...this.state,
      friendSearch: value,
    });

    // searchUsers will be delayed for 300ms, to reduce calls to searchUsers
    await this.searchUsers(value);
  }

  // immediately fires off a reload
  searchUsersImmediately = async (username) => {
    username = username.toLocaleLowerCase();
    try {

      // Get Friends and co. data
      const responseFriends = await fetchFriends();
      const responseRequestSent = await fetchOutgoingFriendRequests();
      const responseRequestIncoming = await fetchIncomingFriendRequests();

      // Data Lists for Friends and co.
      const friends = responseFriends.data.filter(s => s.username.toLocaleLowerCase().includes(username));
      const requestSent = responseRequestSent.data.map(o => o.receiver);
      const requestIncoming = responseRequestIncoming.data.map(o => o.sender);

      // Populate Friends and co. in userTypeMap
      const userTypeMap = new Map();

      friends.forEach((f) => userTypeMap.set(f.id, UT_FRIEND));
      requestSent.forEach((u) => userTypeMap.set(u.id, UT_FRIEND_REQUEST_SENT));
      requestIncoming.forEach((u) => userTypeMap.set(u.id, UT_FRIEND_REQUEST_INCOMING));

      let strangers = [];
      if (username.length >= 3) {
        const searchResponse = await searchUser(username);
        // Remove friends and logged in user
        strangers = searchResponse.data.filter(s => {
            const isFriend = userTypeMap.get(s.id) !== UT_FRIEND;
            const isCurrentUser = s.username !== this.props.username;
            return isFriend && isCurrentUser;
          }
        );
      }

      this.setState({
        ...this.state,
        loading: false,
        error: null,
        friends,
        strangers,
        requestSent,
        requestIncoming,
        userTypeMap,
      });

    } catch (e) {
      this.setState({
          error: JSON.stringify(e),
          loading: false
        }
      );
    }
  };


  // to be called by search,  defers actual reload for 300ms
  searchUsers = debounce(this.searchUsersImmediately, 300);

  async sendFriendRequest(userId) {
    try {
      await createFriendRequest(userId);
      this.props.showToast("Freundschaftsanfrage gesendet.");
      this.reload();
    } catch (e) {
      console.error(e);
      this.setState({
          error: JSON.stringify(e),
          loading: false,
        }
      );
    }
  }

  async acceptFriendRequest(userId) {
    try {
      await acceptFriendRequest(userId);
      this.props.showToast("Freundschaftsanfrage angenommen.");
      this.reload();
    } catch (e) {
      console.error(e);
      this.setState({
          error: JSON.stringify(e),
          loading: false,
        }
      );
    }
  }

  async removeFriendRequest(userId) {
    try {
      await deleteFriendRequest(userId);
      this.props.showToast("Freundschaftsanfrage gelöscht.");
      this.reload();
    } catch (e) {
      console.error(e);
      this.setState({
          error: JSON.stringify(e),
          loading: false,
        }
      );
    }
  }

  async removeFriend(userId) {
    try {
      await deleteFriend(userId);
      this.props.showToast("Freund entfernt");
      this.reload();
    } catch (e) {
      console.error(e);
      this.setState({
          error: JSON.stringify(e),
          loading: false,
        }
      );
    }
  }

  // -------------------
  // UI Stuff
  // -------------------

  render() {
    if (this.state.error != null) {
      return (<View style={styles.container}>
        <Text>Sorry, ein Fehler ist aufgetreten</Text>
        <Text>{this.state.error}</Text>
      </View>);
    }

    if (!this.props.signedIn) {
      return (
        <View style={[{
          display: "flex",
          alignItems: "center",
          justifyContent: "center",
          marginHorizontal: 20
        }, styles.container]}>
          <Paragraph style={{textAlign: "center"}}>
            Du musst dich registrieren um Freunde hinzufügen zu können.
          </Paragraph>
          <Button mode="contained" style={{marginVertical: 20}} accessibilityLabel="Jetzt registrieren"
                  onPress={() => Navigation.navigate("AccountTab", "SignUp")}>
            Jetzt registieren
          </Button>
        </View>
      );
    }

    let viewPane;
    switch (this.state.selectedIndex) {
      case REQUEST_INCOMING_VIEW:
        viewPane = this.renderIncomingScreen();
        break;
      case REQUEST_OUTGOING_VIEW:
        viewPane = this.renderSentScreen();
        break;
      default:
        viewPane = this.renderSearchScreen();
    }

    let renderSearchField;
    if (this.state.selectedIndex === SEARCH_VIEW) {
      renderSearchField = (<View style={styles.searchView}>
        <Searchbar
          accessibilityLabel="Suchleiste"
          style={styles.searchField}
          placeholder="Benutzername..."
          value={this.state.friendSearch}
          onChangeText={this.onSearch.bind(this)}
        />
        <IconButton small
                    style={styles.scannerToggler}
                    icon="qrcode-scan"
                    color={Colors.black}
                    accessibilityLabel="QR-Code eines Benutzers scannen"
                    onPress={this.openScanner.bind(this)}
        />
      </View>);
    }


    return (
      <View style={styles.container}>
        <ScrollView
          keyboardShouldPersistTaps="handled" /* when keyboard is open, allow button press */
          refreshControl={<RefreshControl refreshing={this.state.loading}
                                          onRefresh={this.reload.bind(this)}/>}>

          <SegmentedControlTab
            values={["Suchen", "Anfragen", "Gesendet"]}
            selectedIndex={this.state.selectedIndex}
            onTabPress={this.handleIndexChange}
          />

          {renderSearchField}
          {viewPane}
        </ScrollView>
      </View>

    );
  }

  navigateToUser = (user) => {
    Navigation.navigate("FriendsTab", "FriendDetail", {
      friend: user
    });
  };

  /**
   * Renders a FlatList with the given data as renderItems.
   * The buttons will be displayed on each item.
   *
   * @param data Data to be displayed
   * @param createButtonFunction Function which takes a userId as Argument
   * @returns FlatList
   */
  renderFlatList(data, createButtonFunction) {
    return (
      <FlatList
        keyboardShouldPersistTaps="handled"
        data={data}
        refreshing={this.state.loading}
        onRefreash={this.reload.bind(this)}
        renderItem={({item}) => (
          <TouchableRipple accessibilityLabel={"Benutzer - " + item.username} style={styles.itemTouchableContainer}
                           onPress={() => this.navigateToUser(item)}>
            <View style={styles.itemContainer}>
              <Text style={styles.username}>{item.username}</Text>
              <View style={styles.buttonContainer}>
                {createButtonFunction(item.id)}
              </View>
            </View>
          </TouchableRipple>
        )}
        keyExtractor={item => item.id + ""}
      />
    );
  }

  // -------------------
  // Buttons
  // -------------------
  addFriendButton = (userId) => (
    <IconButton small
                icon="account-multiple-plus"
                color={Colors.green500}
                accessibilityLabel={"Freund hinzufügen - " + userId}
                onPress={() => this.sendFriendRequest(userId)}
                key={userId + "addFriend"}
    />
  );

  confirmFriendRequestButton = (userId) => (
    <IconButton small
                icon="account-check"
                color={Colors.green500}
                accessibilityLabel={"Freundschaftsanfrage bestätigen - " + userId}
                onPress={() => this.acceptFriendRequest(userId)}
                key={userId + "confirmRequest"}
    />
  );

  deleteFriendRequestButton = (userId) => (
    <IconButton small
                icon="account-remove"
                color={Colors.red500}
                accessibilityLabel={"Freundschaftsanfrage löschen - " + userId}
                onPress={() => this.removeFriendRequest(userId)}
                key={userId + "deleteRequest"}
    />
  );

  deleteFriendshipButton = (userId) => (
    <IconButton small
                icon="account-off"
                color={Colors.red500}
                accessibilityLabel={"Freund entfernen - " + userId}
                onPress={() => this.removeFriend(userId)}
                key={userId + "removeFriend"}
    />
  );

  buttonsForStrangerListItem = (userId) => {
    const arr = [];
    const type = this.state.userTypeMap.get(userId);

    if (type === UT_FRIEND_REQUEST_SENT) {
      arr.push(this.deleteFriendRequestButton);
    } else if (type === UT_FRIEND_REQUEST_INCOMING) {
      arr.push(this.confirmFriendRequestButton);
      arr.push(this.deleteFriendRequestButton);
    } else {
      arr.push(this.addFriendButton);
    }

    return (
      arr.map((buttonFn) => {
        return buttonFn(userId);
      }));
  };

  // -------------------
  // Screens
  // -------------------

  renderSearchScreen() {
    const strangerData = this.state.strangers;
    const strangerButtons = this.buttonsForStrangerListItem;

    const friendsData = this.state.friends;
    const friendsButtons = this.deleteFriendshipButton;

    let strangersList;
    if (this.state.strangers.length > 0) {
      strangersList = (<View>
        <Headline style={styles.heading}>Unbekannte Benutzer</Headline>
        {this.renderFlatList(strangerData, strangerButtons)}
      </View>);
    }

    return (
      <View>
        {strangersList}
        <Headline style={styles.heading}>Meine Freunde</Headline>
        {this.renderFlatList(friendsData, friendsButtons)}
      </View>
    );
  }

  renderIncomingScreen() {
    const incomingRequestData = this.state.requestIncoming;

    const arr = [this.confirmFriendRequestButton, this.deleteFriendRequestButton];
    const incomingRequestButtons = (userId) => {
      return arr.map(buttonFn => {
        return buttonFn(userId);
      });
    };

    return (
      <View>
        <Headline style={styles.heading}>Anfragen von andern Nutzern</Headline>
        {this.renderFlatList(incomingRequestData, incomingRequestButtons)}
      </View>
    );
  }

  renderSentScreen() {
    const requestSentData = this.state.requestSent;
    const requestSentButtons = this.deleteFriendRequestButton;
    return (
      <View>
        <Headline style={styles.heading}>Gesendete Anfragen</Headline>
        {this.renderFlatList(requestSentData, requestSentButtons)}
      </View>
    );
  }

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 10,
  },
  searchField: {
    marginTop: 20,
  },
  itemTouchableContainer: {
    marginTop: 8,
    marginBottom: 8,
    paddingTop: 10,
    paddingBottom: 10,
    paddingLeft: 10,
    borderBottomColor: "lightgrey",
    borderTopColor: "lightgrey",
    borderBottomWidth: 1,
    borderTopWidth: 1,
    height: 60,
    display: "flex",
    justifyContent: "center"
  },
  itemContainer: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
  buttonContainer: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
  heading: {
    marginTop: 10,
  },
  username: {
    fontSize: 18,
  },
  searchView: {
    display: "flex",
    flexDirection: "row",
    alignItems: "flex-end",
    justifyContent: "flex-end"
  },
  scannerToggler: {}
});

const mapStateToProps = state => {
  return {
    signedIn: !state.account.account.pseudonymous,
    username: state.account.account.username
  };
};

const mapDispatchToProps = dispatch => bindActionCreators({
  showToast,
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(FriendsScreen);