import React from "react";
import FriendsScreen from "../FriendsScreen";
import {fireEvent, waitForElement, waitForElementToBeRemoved} from "@testing-library/react-native";
import {createEmptyTestStore, createNetworkMock, renderScreenForTest} from "../../../test/test-utils";
import Navigation from "../../../services/Navigation";

const mock = createNetworkMock();
const store = createEmptyTestStore();

function changeUser(user) {
  store.dispatch({
    type: "ACCOUNT_UPDATE",
    account: user
  });
}

// ------------------------------
// Users
// ------------------------------
const userMarc = {
  username: "msi",
  email: "test@email.com",
  first_name: "Marc",
  id: 999,
  token: "testtoken",
  pseudonymous: false,
};

const userFritz = {
  email: "Fritz@fritz.ch",
  first_name: "Fritz",
  id: 1,
  pseudonymous: false,
  username: "fritz",
};
const userJohn = {
  email: "john@doe.com",
  first_name: "John",
  id: 2,
  pseudonymous: false,
  username: "johnny",
};
const userFrodo = {
  email: "frodo.baggins@test.com",
  first_name: "Frodo",
  id: 3,
  pseudonymous: false,
  username: "frodo",
};
const userBilbo = {
  email: "bilbo.baggins@test.com",
  first_name: "Bilbo",
  id: 4,
  pseudonymous: false,
  username: "bilbo",
};
const userLegolas = {
  email: "legolas.greenleaf@test.com",
  first_name: "Legolas",
  id: 5,
  pseudonymous: false,
  username: "legolas",
};
const notFriendYet = {
  email: "notFriendYet.notFriendYet@test.com",
  first_name: "notFriendYet",
  id: 6,
  pseudonymous: false,
  username: "notFriendYet",
};

// ------------------------------
// Basic Responses
// ------------------------------
const friendsResponse = [
  userFrodo,
  userBilbo,
];
const friendRequestsResponses = [{
  date: "2020-02-29T17:54:29.418176+01:00",
  id: 2,
  receiver: userFritz,
  sender: userMarc
}];
const friendRequestsResponses2 = friendRequestsResponses.slice();
friendRequestsResponses2.push({
  date: "2020-02-29T17:54:29.418176+01:00",
  id: 3,
  receiver: notFriendYet,
  sender: userJohn
});

const friendRequestsIncomingResponses = [{
  date: "2020-01-01T01:00:00+01:00",
  id: 1,
  receiver: userMarc,
  sender: userLegolas,
}];


function basicMockSetup(mock) {
  mock.onGet("/account/friends").reply(200, friendsResponse);
  mock.onGet("/account/friendrequest?direction=out").reply(200, friendRequestsResponses);
  mock.onGet("/account/friendrequest?direction=in").reply(200, friendRequestsIncomingResponses);
}

const TAB_INCOMING_REQ = "Anfragen";
const TAB_SEARCH = "Suchen";
const TAB_SENT_REQ = "Gesendet";

describe("FriendsScreen", () => {

  beforeEach(async () => {
    mock.reset();
    basicMockSetup(mock);
    changeUser(userMarc);
    Navigation.navigate.mockClear();
  });

  it(`shows own qr code`, async () => {
    // Wait for a element to be sure the whole screen is rendered
    const {getByLabelText, asJSON} = renderScreenForTest(FriendsScreen, store);
    const btn = await waitForElement(() => getByLabelText("Eigenen QR-Code anzeigen"));
    fireEvent.press(btn);
    expect(Navigation.navigate.mock.calls.length).toBe(1);
    expect(Navigation.navigate.mock.calls[0][0]).toBe("FriendsTab");
    expect(Navigation.navigate.mock.calls[0][1]).toBe("AccountCode");
  });

    it(`opens AccountCodeScreen`, async () => {
    // Wait for a element to be sure the whole screen is rendered
    const {getByLabelText, asJSON} = renderScreenForTest(FriendsScreen, store);
    const btn = await waitForElement(() => getByLabelText("QR-Code eines Benutzers scannen"));
    fireEvent.press(btn);
    expect(Navigation.navigate.mock.calls.length).toBe(1);
    expect(Navigation.navigate.mock.calls[0][0]).toBe("FriendsTab");
    expect(Navigation.navigate.mock.calls[0][1]).toBe("ScanFriendCode");
  });

  it(`user not logged in`, async () => {
    changeUser({pseudonymous: true});

    const {getByLabelText, asJSON} = renderScreenForTest(FriendsScreen, store);
    const registerBtn = await waitForElement(() => getByLabelText("Jetzt registrieren"));
    fireEvent.press(registerBtn);
    expect(mock.history.get.length).toBe(0);
    expect(Navigation.navigate).toHaveBeenCalledTimes(1);
    expect(Navigation.navigate.mock.calls[0][0]).toBe("AccountTab");
    expect(Navigation.navigate.mock.calls[0][1]).toBe("SignUp");
    expect(asJSON()).toMatchSnapshot();
  });

  it(`renders basic screen`, async () => {
    mock.reset();
    basicMockSetup(mock);

    // Wait for a element to be sure the whole screen is rendered
    const {getByLabelText, asJSON} = renderScreenForTest(FriendsScreen, store);
    await waitForElement(() => getByLabelText("Freund entfernen - " + userBilbo.id));

    expect(asJSON()).toMatchSnapshot();
    expect(mock.history.get.length).toBe(3);
  });

  /**
   * Additional information: Own user can never be a friend of his own, nor can have friendrequest
   * linked to himself.
   */
  it(`check own user not rendered while searching`, async () => {
    const me = userMarc;
    const searchString = userMarc.username;
    const buttonString = "Benutzer - " + me.username;

    const {getByLabelText} = renderScreenForTest(FriendsScreen, store);
    const searchbar = await waitForElement(() => getByLabelText("Suchleiste"));
    fireEvent.changeText(searchbar, searchString);

    try {
      await waitForElement(() => getByLabelText(buttonString));
      fail("Own user should not be shown in stranger list");
    } catch (e) { /* expect(...).toThrow() does not work here somehow */
    }
  });

  it(`navigate to user detail screen`, async () => {
    const user = friendsResponse[0];
    const buttonString = "Benutzer - " + user.username;

    const {getByLabelText, asJSON} = renderScreenForTest(FriendsScreen, store);
    const button = await waitForElement(() => getByLabelText(buttonString));
    fireEvent.press(button);

    expect(Navigation.navigate.mock.calls[0][0]).toBe("FriendsTab");
    expect(Navigation.navigate.mock.calls[0][1]).toBe("FriendDetail");
    expect(mock.history.get.length).toBe(3);
    expect(Navigation.navigate).toHaveBeenCalledTimes(1);
    expect(asJSON()).toMatchSnapshot();
  });

  it(`send friend request`, async () => {
    const searchStr = "notF";
    const buttonStringAdd = "Freund hinzufügen - " + notFriendYet.id;
    const buttonStringDelete = "Freundschaftsanfrage löschen - " + notFriendYet.id;

    mock.onGet("/accounts/?query=" + searchStr.toLocaleLowerCase()).reply(200, [notFriendYet]);
    mock.onPost("/account/friendrequest").reply(201, {}); // Data no used

    // Search
    const {getByLabelText, asJSON} = renderScreenForTest(FriendsScreen, store);
    const searchbar = await waitForElement(() => getByLabelText("Suchleiste"));
    fireEvent.changeText(searchbar, searchStr);
    const sendRequestButton = await waitForElement(() => getByLabelText(buttonStringAdd));
    expect(asJSON()).toMatchSnapshot();

    // Add as Friend
    fireEvent.press(sendRequestButton);
    mock.onGet("/account/friendrequest?direction=out").reply(200, friendRequestsResponses2);
    const removeRequest = await waitForElement(() => getByLabelText(buttonStringDelete));

    expect(mock.history.post.length).toBe(1);
    expect(mock.history.get.length).toBe(11);
    expect(asJSON()).toMatchSnapshot();
  });

  it(`accept incoming friend request`, async () => {
    const sender = friendRequestsIncomingResponses[0].sender;
    const buttonString = "Freundschaftsanfrage bestätigen - " + sender.id;
    const newFriends = friendsResponse.slice();
    newFriends.push(sender);

    mock.onGet("/account/friendrequest?direction=in").reply(200, friendRequestsIncomingResponses);
    mock.onPut("/account/friendrequest/" + sender.id).reply(201, {});// Data not used

    // Select Anfragen
    const {getByLabelText, asJSON} = renderScreenForTest(FriendsScreen, store);
    const incomingSection = await waitForElement(() => getByLabelText(TAB_INCOMING_REQ));
    fireEvent.press(incomingSection);
    const confirmRequestButton = await waitForElement(() => getByLabelText(buttonString));

    mock.onGet("/account/friendrequest?direction=in").reply(200, []);
    fireEvent.press(confirmRequestButton);
    await waitForElementToBeRemoved(() => getByLabelText(buttonString));

    expect(mock.history.put.length).toBe(1);
    expect(mock.history.get.length).toBe(6);
    expect(asJSON()).toMatchSnapshot();
  });

  it(`decline incoming friend request`, async () => {
    const toRemove = friendRequestsIncomingResponses[friendRequestsIncomingResponses.length - 1].sender;
    const buttonString = "Freundschaftsanfrage löschen - " + toRemove.id;
    const newFriendRequestsIncomingResponses = friendRequestsIncomingResponses.slice();
    newFriendRequestsIncomingResponses.pop();

    mock.onDelete("/account/friendrequest/" + toRemove.id).reply(200, {});

    // Navigate to section
    const {getByLabelText, asJSON} = renderScreenForTest(FriendsScreen, store);
    const incomingSection = await waitForElement(() => getByLabelText(TAB_INCOMING_REQ));
    fireEvent.press(incomingSection);

    // Click delete Button
    const deleteRequestButton = await waitForElement(() => getByLabelText(buttonString));
    mock.onGet("/account/friendrequest?direction=in").reply(200, newFriendRequestsIncomingResponses);
    fireEvent.press(deleteRequestButton);
    await waitForElementToBeRemoved(() => getByLabelText(buttonString));

    expect(mock.history.delete.length).toBe(1);
    expect(mock.history.get.length).toBe(6);
    expect(asJSON()).toMatchSnapshot();
  });

  it(`remove sent friend request`, async () => {
    const toRemove = friendRequestsResponses[0].receiver;
    const buttonString = "Freundschaftsanfrage löschen - " + toRemove.id;

    mock.onDelete("/account/friendrequest/" + toRemove.id).reply(200, {});

    const {getByLabelText, asJSON} = renderScreenForTest(FriendsScreen, store);
    const section = await waitForElement(() => getByLabelText(TAB_SENT_REQ));
    fireEvent.press(section);

    const deleteButton = await waitForElement(() => getByLabelText(buttonString));
    mock.onGet("/account/friendrequest?direction=out").reply(200, []);
    fireEvent.press(deleteButton);
    await waitForElementToBeRemoved(() => getByLabelText(buttonString));

    expect(mock.history.get.length).toBe(6);
    expect(mock.history.delete.length).toBe(1);
    expect(asJSON()).toMatchSnapshot();
  });

  it(`remove friend`, async () => {
    const toRemove = friendsResponse[friendsResponse.length - 1];
    const buttonString = "Freund entfernen - " + toRemove.id;
    const newFriendsResponse = friendsResponse.slice();
    newFriendsResponse.pop();

    mock.onDelete("/account/friends/" + toRemove.id).reply(200, {}); // Data not needed

    const {getByLabelText, asJSON} = renderScreenForTest(FriendsScreen, store);
    const deleteButton = await waitForElement(() => getByLabelText(buttonString));
    mock.onGet("/account/friends").reply(200, newFriendsResponse);
    fireEvent.press(deleteButton);
    await waitForElementToBeRemoved(() => getByLabelText(buttonString));

    expect(mock.history.get.length).toBe(6);
    expect(mock.history.delete.length).toBe(1);
    expect(asJSON()).toMatchSnapshot();
  });

});