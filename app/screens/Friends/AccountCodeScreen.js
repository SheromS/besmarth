import React from "react";
import {Dimensions, StyleSheet, View} from "react-native";
import {Button, Paragraph} from "react-native-paper";
import {getAccountCodeImage} from "../../services/AccountService";
import Navigation from "../../services/Navigation";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";

function AccountCodeScreen(props){
    const codeWidth = Dimensions.get("window").width - 20;
    return <View style={[styles.container, {display: "flex", alignItems: "center", justifyContent: "center"}]}>
      <Paragraph>Lass den Code von deinen Freunden scannen, damit Sie deine Challenges sehen.</Paragraph>
      <View style={{marginVertical: 20}}>
        {getAccountCodeImage(props.username, codeWidth, codeWidth)}
      </View>
      <Button mode="contained" accessibilityLabel="Zurück" onPress={() => Navigation.goBack()}>Zurück</Button>
    </View>;
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 10,
  }
});


const mapStateToProps = state => {
  return {
    username: state.account?.account?.username
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators({}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(AccountCodeScreen);
