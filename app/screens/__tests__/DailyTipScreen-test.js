import React from "react";
import DailyTipScreen from "../DailyTipScreen";

import {createEmptyTestStore, renderForTest, renderScreenForTest} from "../../test/test-utils";
import {createStackNavigator} from "@react-navigation/stack";
import {waitForElement} from "@testing-library/react-native";
import {NavigationContainer} from "@react-navigation/native";
import {DAILY_TIP_LAST_SEEN_CACHE_KEY, hasUserSeenDailyTip} from "../../services/DailyTipService";
import LocalStorage from "../../services/LocalStorage";


describe("DailyTip", () => {
  it(`renders`, () => {
    const store = createEmptyTestStore();
    store.dispatch({
      type: "RECEIVE_DAILY_TIP",
      dailyTip: {
        title: "Test Daily Tip Title",
        content: "Daily Tip Content 32 52$¨3452ö 523 232",
        image: "http://test.local/image/url"
      }
    });
    const {asJSON} = renderScreenForTest(DailyTipScreen, store);
    expect(asJSON()).toMatchSnapshot();
  });

  it(`render in navigation context`, async () => {
    // user has not seen daily tip yet
    await LocalStorage.deleteCacheItem(DAILY_TIP_LAST_SEEN_CACHE_KEY);
    expect(await hasUserSeenDailyTip()).toBe(false);
    const store = createEmptyTestStore();
    store.dispatch({
      type: "RECEIVE_DAILY_TIP",
      dailyTip: {
        title: "Test Daily Tip Title",
        content: "Daily Tip Content 32 52$¨3452ö 523 232",
        image: "http://test.local/image/url"
      }
    });
    const Stack = createStackNavigator();
    // render the daily tip screen inside a navigation context
    const {getByLabelText, asJSON} = renderForTest(<NavigationContainer>
      <Stack.Navigator><Stack.Screen name="DailyTip"
        component={DailyTipScreen}/></Stack.Navigator></NavigationContainer>, store);

    await waitForElement(() => getByLabelText("Tipp des Tages"));
    expect(await hasUserSeenDailyTip()).toBe(true);
    expect(asJSON()).toMatchSnapshot();
  });
})
;
