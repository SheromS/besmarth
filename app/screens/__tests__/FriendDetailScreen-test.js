import React from "react";
import {
  createEmptyTestStore,
  createNetworkMock,
  mockNavigation,
  renderForSnapshot, renderForTest, waitForReduxAction,
} from "../../test/test-utils";
import FriendDetailScreen from "../FriendDetailScreen";
import {waitForElement} from "@testing-library/react-native";
//import {withNavigation} from "react-navigation";


const mock = createNetworkMock();

const friend = {
  "email": "max@muster.com",
  "first_name": "Max",
  "id": 22,
  "pseudonymous": false,
  "username": "Max",
};

const challenge = {
  id: 134515,
  title: "Challenge Title",
  image: "http://test.local/image/url",
  color: "#abcdef",
  periodicity: "daily",
  duration: 100
};

describe("FriendDetailScreen", () => {
  it(`dummy`,  () => {
    expect(true).toBe(true);
  })
/*
  it(`shows challenge progress`, async () => {
    mock.reset();
    mock.onGet("/friends/22/challenges").reply(200, [
      {
        "challenge": {
          "title": "Shut Exe",
          "duration": 60,
        },
        "mark_deleted": false,
        "progress": [
          {},{},{},{},{},{},{},{},
        ],
        "shared": true,
      },
      {
        "challenge": {
          "title": "Mug-hug",
          "duration": 30,
        },
        "mark_deleted": false,
        "progress": [
          {},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},
        ],
        "shared": true,
      },
    ]);

    const store = createEmptyTestStore();

    const {getByTestId, asJSON} = renderForTest(withNavigation(<FriendDetailScreen navigation={mockNavigation({ friend })}/>), store);

    await waitForElement(() => getByTestId("view"));
    expect(asJSON()).toMatchSnapshot();
  });

  it(`shows "X times completed" after at least one full completion`, async () => {
    mock.reset();
    mock.onGet("/friends/22/challenges").reply(200, [
      {
        "challenge": {
          "title": "Mug-hug",
          "duration": 30,
        },
        "mark_deleted": false,
        "progress": [
          {},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},
        ],
        "shared": true,
      },
    ]);

    const store = createEmptyTestStore();

    const {getByTestId, asJSON} = renderForTest(withNavigation(<FriendDetailScreen navigation={mockNavigation({ friend })}/>), store);

    await waitForElement(() => getByTestId("view"));
    expect(asJSON()).toMatchSnapshot();
  });

  it(`don't show unshared challenges`, async () => {
    mock.reset();
    mock.onGet("/friends/22/challenges").reply(200, [
      {
        "challenge": {
          "title": "Mug-hug",
          "duration": 30,
        },
        "mark_deleted": false,
        "progress": [
          {},{},{},
        ],
        "shared": false,
      },
    ]);

    const store = createEmptyTestStore();

    const {getByTestId, asJSON} = renderForTest(withNavigation(<FriendDetailScreen navigation={mockNavigation({ friend })}/>), store);

    await waitForElement(() => getByTestId("view"));
    expect(asJSON()).toMatchSnapshot();
  });

  it(`show message when no challenges are shared`, async () => {
    mock.reset();
    mock.onGet("/friends/22/challenges").reply(200, []);

    const store = createEmptyTestStore();
    const {getByTestId, asJSON} = renderForTest(withNavigation(<FriendDetailScreen navigation={mockNavigation({ friend })}/>), store);
    await waitForElement(() => getByTestId("view"));
    expect(asJSON()).toMatchSnapshot();
  });
  // TODO backend test for not friend
  */
});



/*
    [
      {
        "challenge": {
          "color": "#0B85B5",
          "description": "Schalte den Monitor aus, statt den Bildschirmschoner einzustellen.",
          "difficulty": "EASY",
          "duration": 60,
          "icon": null,
          "id": 4,
          "image": "/media/images/challenges/shut-eye.jpeg",
          "periodicity": "DAILY",
          "title": "Shut Exe",
          "topic": 4,
        },
        "id": 6,
        "joined_time": "2019-12-17T21:46:48.974088+01:00",
        "mark_deleted": false,
        "progress": [
          {
            "create_time": "2019-12-17T21:46:50.948999+01:00",
            "id": 28,
            "mark_deleted": false,
            "participation": 6,
          },
        ],
        "shared": true,
      },
      {
        "challenge": {
          "color": "#30B50B",
          "description": "Benutze deine eigene Tasse, wenn du Kaffeetrinken gehst",
          "difficulty": "EASY",
          "duration": 30,
          "icon": "/media/images/challenges/background-2277_1920_4LZmPvl.jpg",
          "id": 2,
          "image": "/media/images/challenges/mug-hug.jpg",
          "periodicity": "DAILY",
          "title": "Mug-hug",
          "topic": 3,
        },
        "id": 7,
        "joined_time": "2019-12-17T21:47:03.317022+01:00",
        "mark_deleted": false,
        "progress": [
          {
            "create_time": "2019-12-17T21:47:04.979162+01:00",
            "id": 29,
            "mark_deleted": false,
            "participation": 7,
          },
        ],
        "shared": true,
      },
    ]
 */