import React from "react";
import {StyleSheet, View, ScrollView, ActivityIndicator, Text} from "react-native";
import {fetchAchievedRewards} from "../services/Reward";
import {RewardItem} from "../components/RewardItem";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {notNull} from "../services/utils";


class RewardScreen extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      loading: true
    };
  }

  componentDidMount() {
    this.subscription = this.props.navigation.addListener("focus", this.reload.bind(this));
    this.reload();
  }

  componentWillUnmount() {
    this.subscription();
  }

  async reload() {
    try {
      const achievedRewards = await fetchAchievedRewards();
      this.setState({
        loading: false,
        achievedRewards
      });
    } catch (e) {
      // 404 happens when the user did not already signed up for the challenge
      if (e.status !== 404) {
        throw new Error("Unerwarteter Fehler beim Laden", e);
      } else {
        this.setState({
          loading: false,
        });
        return null;
      }
    }
  }

  render() {

      if (this.state.loading) {
        return <View style={styles.container}><ActivityIndicator size="large"/></View>;
      }
      if(this.state.error){
        return <View style={styles.container}><Text>{this.state.error}</Text></View>
      }

      const achievedRewards = this.state.achievedRewards;
      if(notNull(achievedRewards) && achievedRewards.length > 0) {

        let items = achievedRewards.map((achievedReward, key) => {
            return (<RewardItem reward={achievedReward.reward} key={key} />);
        });

        let totalPoints = 0;
        for(let r in achievedRewards){
          totalPoints += achievedRewards[r].reward.points;
        }

        return (
          <View>
            <ScrollView style={styles.scrollview}>
              {items}
            </ScrollView>
            <Text style={styles.sum}>Insgesammte Punkte: {totalPoints}</Text>
            </View>
        );
      } else {
        return (
          <View>
            <Text>Du hast leider noch keine Errungenschaften freigeschalten</Text>
          </View>
        );
    }
  }


}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  scrollview: {
    borderBottomColor: 'black',
    borderBottomWidth: 1,
  },
});


const mapStateToProps = state => {
  return {
  };
};

const mapDispatchToProps = dispatch =>
    bindActionCreators({}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(RewardScreen);