import React from "react";
import {ScrollView, StyleSheet, View} from "react-native";
import {bindActionCreators} from "redux";
import {
  changeSharedStateChallenge,
  createChallengeProgress,
  fetchChallengeParticipation,
  getNumberOfCompletedChallenges,
  progressButtonAvailable,
  signUpChallenge,
  unfollowChallenge
} from "../services/Challenges";
import {connect} from "react-redux";
import {notNull} from "services/utils";
import {ActivityIndicator, Button, Card, FAB, Text} from "react-native-paper";
import {showToast} from "services/Toast";
import {ChallengeProgress} from "components/Challenge/ChallengeProgress";
import {ChallengeInfo} from "components/Challenge/ChallengeInfo";
import {Colors, Buttons} from  "styles/index";

class ChallengeDetailScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
    };
  }

  componentDidMount() {
    this.reload().then();
  }

  async reload() {
    try {
      const challenge = this.props?.route?.params?.challenge;
      this.props.navigation.setOptions({
        title: challenge?.title
      });
      this.setState({loading: true, challenge});
      const participation = await fetchChallengeParticipation(challenge);
      this.setState({loading: false, challenge, participation});
    } catch (e) {
      console.log(e);
      this.setState({loading: false, error: "Ein Fehler ist aufgetreten"});
    }
  }

  buildLogProgressButton() {
    if (progressButtonAvailable(this.state.participation)) {
      return (
        <FAB
          style={Buttons.challengeFab}
          label="Geschafft"
          mode="contained"
          accessibilityLabel="Challenge als erledigt markieren"
          onPress={this.onLogProgress.bind(this)}/>
      );
    } else {
      return null;
    }
  }

  async onSignUpChallenge() {
    const participation = await signUpChallenge(this.state.challenge);
    this.setState({
      ...this.state,
      participation
    });
    this.props.showToast("Gratuliere! Du hast dich erfolgreich für " + this.state.challenge.title + " angemeldet");
  }

  async onLogProgress() {
    this.setState({
      ...this.state,
      participation: await createChallengeProgress(this.state.challenge)
    });

    if (this.state.participation.progress.length % this.state.challenge.duration === 0) {
      this.props.showToast("Gratuliere! Du hast die Challenge " + this.state.challenge.title + " erfolgreich abgeschlossen");
      this.reload();
    }
  }

  async unfollowChallenge() {
    this.setState({
      ...this.state,
      participation: await unfollowChallenge(this.state.challenge)
    });
    this.props.showToast("Challenge deabonniert.");
    this.reload();
  }

  async onShareChallenge() {
    this.setState({
      ...this.state,
      participation: await changeSharedStateChallenge(this.state.challenge, true)
    });
    this.props.showToast("Deine Freunde können deine Fortschritt dieser Challenge nun sehen");
  }

  async onUnshareChallenge() {
    this.setState({
      ...this.state,
      participation: await changeSharedStateChallenge(this.state.challenge, false)
    });
    this.props.showToast("Deine Freunde können deinen Fortschritt nicht mehr sehen.");
  }

  /**
   * Returns true if the user has already signed up for this challenge
   */
  isChallengeAlreadyStarted() {
    return notNull(this.state.participation) && notNull(this.state.participation?.joined_time) && !this.state.participation?.mark_deleted;
  }

  buildSharingButton() {
    if (this.state.participation?.shared) {
      return <Button
        label="Teilen aufheben"
        accessibilityLabel="Teilen aufheben"
        style={styles.button}
        mode="outlined"
        onPress={this.onUnshareChallenge.bind(this)}>Teilen aufheben</Button>;
    } else {
      return <Button
        accessibilityLabel="Mit Freunden teilen"
        style={styles.button}
        mode="outlined"
        onPress={this.onShareChallenge.bind(this)}>Mit Freunden teilen</Button>;
    }
  }

  render() {
    if (this.state.loading) {
      return <ActivityIndicator size="large"/>;
    }

    if (this.state.error) {
      return <Text>{this.state.error}</Text>;
    }

    let progressOrSignInView;
    if (this.isChallengeAlreadyStarted()) {
      progressOrSignInView = (
        <View>
          <Card>
            <Card.Title title="Dein Fortschritt"/>
            <Card.Content>
              <ChallengeProgress participation={this.state.participation}
                                 challenge={this.state.challenge}></ChallengeProgress>
              {this.buildLogProgressButton()}
            </Card.Content>
          </Card>
          <Card style={styles.challengeCard}>
            <Card.Content>
              {ChallengeProgress.countCompletedProgress(getNumberOfCompletedChallenges(this.state.participation, this.state.challenge))}
            </Card.Content>
          </Card>
          <Card>
            <Card.Content>
              <FAB
                style={styles.button}
                label="Deabonnieren"
                mode="contained"
                accessibilityLabel="Challenge deabonnieren"
                testID="Challenge deabonnieren"
                onPress={this.unfollowChallenge.bind(this)}/>
              {this.buildSharingButton()}
            </Card.Content>
          </Card>
        </View>
      );
    } else {
      progressOrSignInView = (
        <View style={styles.card}>
          <FAB
            style={styles.button}
            mode="contained"
            label="Teilnehmen"
            accessibilityLabel="Für Challenge anmelden"
            onPress={this.onSignUpChallenge.bind(this)}/>

          <Card style={styles.challengeCard}>
            <Card.Content>
              {ChallengeProgress.countCompletedProgress(getNumberOfCompletedChallenges(this.state.participation, this.state.challenge))}
            </Card.Content>
          </Card>

        </View>
      );
    }
    return (
      <View style={styles.container}>
        <ScrollView>
          <ChallengeInfo challenge={this.state.challenge}/>
          {progressOrSignInView}
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  button: {
    marginHorizontal: 10,
    marginVertical: 10,
  },
  challengeCard: {
    borderStyle: "solid",
    borderColor: "white",
    borderWidth: 1,
  }
});

const mapStateToProps = state => {
  return {};
};
const mapDispatchToProps = dispatch => bindActionCreators({
  showToast,
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(ChallengeDetailScreen);
