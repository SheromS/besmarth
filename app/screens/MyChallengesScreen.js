import React from "react";
import {FlatList, StyleSheet, View} from "react-native";
import {bindActionCreators} from "redux";
import {FAB, Headline, Surface, Text, TouchableRipple} from "react-native-paper";
import {connect} from "react-redux";
import {createChallengeProgress, fetchOwnChallengeParticipations} from "../services/Challenges";
import Navigation from "../services/Navigation";

class MyChallengesScreen extends React.Component {

  constructor() {
    super();
    this.state = {
      loading: true
    };
  }

  componentDidMount() {
    this.subscription = this.props.navigation.addListener("focus", this.reload.bind(this));
    this.reload().then();
  }

  componentWillUnmount() {
    this.subscription();
  }


  async reload() {
    this.setState({loading: true});
    try {
      const challengeParticipations = await fetchOwnChallengeParticipations();

      this.setState({
        loading: false,
        challengeParticipations
      });
    } catch (e) {
      console.log(e);
      this.setState({
        error: JSON.stringify(e),
        loading: false
      });
    }
  }

  render() {
    if (this.state.error) {
      return <View style={styles.container}><Text>{this.state.error}</Text></View>;
    }

    const emptyContent = <Headline style={styles.emptyListWrapper}>Du nimmst leider noch an keinen Challenges
      teil!</Headline>;

    return (<View style={styles.container}>
      <FlatList
        testID="my-challenges-list"
        style={styles.participationList}
        data={this.state.challengeParticipations}
        onRefresh={this.reload.bind(this)}
        refreshing={this.state.loading}
        keyExtractor={(item) => item.id + ""}
        ListEmptyComponent={() => emptyContent}
        renderItem={({item}) => <ParticipationItem participation={item} onPressCard={this.onOpenChallenge}
                                                   onLogProgress={this.onLogProgress.bind(this)}/>}
      />
    </View>);
  }

  async onOpenChallenge(participation) {
    Navigation.push("MyChallengesTab", "ChallengeDetail", {challenge: participation.challenge});
  }

  async onLogProgress(participation) {
    const updatedParticipation = await createChallengeProgress(participation.challenge);
    this.reload().then();

    if (updatedParticipation.progress.length === updatedParticipation.challenge.duration) {
      this.props.showToast("Gratuliere! Du hast die Challenge " + updatedParticipation.challenge.title + " erfolgreich abgeschlossen");
    }
  }
}

function ParticipationItem({participation, onPressCard, onLogProgress}) {
  let rightAlignedItem;
  if (participation.progress_loggable) {
    rightAlignedItem = <FAB
      style={styles.logProgressButton}
      small
      onPress={() => onLogProgress(participation)}
      icon="check"/>;
  } else {
    const completeness = Math.round(participation.progress.length / participation.challenge.duration * 100);
    rightAlignedItem = <Text>
      {completeness}%
    </Text>;
  }


  return <TouchableRipple style={styles.touchableCard} onPress={() => onPressCard(participation)}>
    <Surface style={styles.card}>
      <View style={styles.cardTitle}>
        <View style={{flex: 1}}>
          <Headline>{participation.challenge.title}</Headline>
        </View>
        <View style={{flex: 0}}>
          <Text style={styles.category}>{participation.challenge.topic}</Text>
        </View>
      </View>
      <View style={styles.participationContent}>
        <View style={{flex: 1}}>
          <Text>{participation.challenge.description}</Text>
        </View>
        <View style={styles.logProgressView}>{rightAlignedItem}</View>
      </View>
    </Surface>
  </TouchableRipple>;
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
  },
  participationList: {
    flex: 1,
    width: "100%"
  },
  touchableCard: {
    margin: 8,
    marginHorizontal: 6,
  },
  card: {
    display: "flex",
    flexDirection: "column",
    alignItems: "stretch",
    justifyContent: "space-between",
    elevation: 4,
    padding: 6,
  },
  cardTitle: {
    display: "flex",
    flexDirection: "row",
    alignItems: "flex-start",
    justifyContent: "flex-end"
  },
  category: {
    backgroundColor: "#f1c40f",
    color: "black",
    paddingVertical: 3,
    paddingHorizontal: 6,
    marginTop: -6,
    marginRight: -6
  },
  logProgressView: {
    flex: 0,
    margin: 5
  },
  logProgressButton: {
    backgroundColor: "#ffd60d",
  },
  participationContent: {
    flex: 1,
    display: "flex",
    flexDirection: "row",
    alignItems: "flex-end",
    marginTop: 10
  },
  emptyListWrapper: {
    marginTop: "30%",
    marginHorizontal: 10,
    textAlign: "center"
  }
});

const mapStateToProps = state => {
  return {};
};

const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(MyChallengesScreen);
