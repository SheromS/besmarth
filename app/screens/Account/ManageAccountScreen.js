import React from "react";
import {AsyncStorage, ScrollView, StyleSheet, View} from "react-native";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import {downloadAndOpenAccountData, loadAccount} from "../../services/AccountService";
import AccountForm from "../../components/AccountForm";
import {Button, Caption, Paragraph} from "react-native-paper";
import LocalStorage from "../../services/LocalStorage";
import {isDevelopmentMode, notNull} from "../../services/utils";
import Navigation from "../../services/Navigation";
import {showToast} from "../../services/Toast";
import ErrorBox from "../../components/ErrorBox";

class ManageAccountScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    (async () => {
      try {
        await this.props.loadAccount();
      } catch (e) {
        console.log("An error happenend: ", e);
        this.setState({
          errors: e
        });
      }
    })();
  }

  async debugLocalStorage() {
    console.log(await LocalStorage.dump());
  }

  async exportAccountData() {
    try {
      await downloadAndOpenAccountData();
    } catch (e) {
      console.log(e);
      this.props.showToast("Das Anfordern der persönlichen Daten ist fehlgeschlagen. Bitte wende dich an den Support um das Problem zu beheben.", "ERROR");
    }
  }

  render() {
    let debugView;

    if (isDevelopmentMode()) {
      debugView = (<View style={{paddingHorizontal: 8, backgroundColor: "lightyellow"}}>
        <Button style={styles.button} mode="contained" onPress={this.debugLocalStorage.bind(this)}>Debug
          LocalStorage</Button>
        <Button style={styles.button} mode="contained" onPress={async () => await LocalStorage.clearCache()}>Delete all local
          data</Button>
      </View>);
    }

    if (notNull(this.state.errors)) {
      return <ErrorBox errors={this.state.errors}/>;
    }

    const exportView = <Button style={styles.button} accessibilityLabel="Meine Daten anfordern" mode="contained"
                               onPress={this.exportAccountData.bind(this)}>Meine Daten anfordern</Button>;

    if (this.props.signedIn) {
      return (
        <ScrollView keyboardShouldPersistTaps="handled" style={styles.container}>
          <AccountForm signUpMode={false} submitTitle="Speichern"/>
          {exportView}
          {debugView}
          <Button onPress={()=> Navigation.navigate("Rewards")} >Rewards</Button>
        </ScrollView>
      );
    } else {
      return (
        <ScrollView contentContainerStyle={styles.chooseContainer}>
          <Caption style={{marginBottom: 10}}>Du bist momentan noch anonym unterwegs.</Caption>
          <Button style={styles.button} mode="contained"
                  onPress={() => Navigation.navigate("AccountTab", "SignIn")}>Login</Button>
          <Paragraph style={{marginVertical: 10}}>oder</Paragraph>
          <Button style={styles.button} mode="contained" onPress={() => Navigation.navigate("AccountTab", "SignUp")}>Account
            erstellen</Button>
          {exportView}
          {debugView}

        </ScrollView>
      );

    }
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  chooseContainer: {
    flex: 1,
    paddingVertical: 100,
    justifyContent: "center",
    alignItems: "center"
  },
  button: {
    marginVertical: 8,
    minWidth: "50%"
  },
  debugButton: {
    marginHorizontal: 10,
  }
});

const mapStateToProps = state => {
  return {
    signedIn: !state.account.account.pseudonymous
  };
};

const mapDispatchToProps = dispatch => bindActionCreators({
  loadAccount,
  showToast
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(ManageAccountScreen);