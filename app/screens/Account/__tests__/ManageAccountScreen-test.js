import React from "react";
import ManageAccountScreen from "../ManageAccountScreen";
import {createEmptyTestStore, createNetworkMock, renderScreenForTest} from "../../../test/test-utils";
import {fireEvent, waitForElement} from "@testing-library/react-native";
import * as FileSystem from "expo-file-system";
import * as Sharing from "expo-sharing";
import RemoteStorage from "../../../services/RemoteStorage";

const mock = createNetworkMock();
mock.onGet("/account").reply(200, {
  username: "test",
  email: "test@email.com",
  first_name: "Samuel",
  pseudonymous: false
});

jest.mock("expo-file-system", () => ({
  downloadAsync: jest.fn()
}));
jest.mock("expo-sharing", () => ({
  shareAsync: jest.fn()
}));

describe("ManageAccountScreen", () => {
  it(`renders logged out`, () => {
    const store = createEmptyTestStore();
    const {asJSON} = renderScreenForTest(ManageAccountScreen, store);
    expect(asJSON()).toMatchSnapshot();
  });
  it(`renders logged in`, () => {
    const store = createEmptyTestStore();

    const {asJSON} = renderScreenForTest(ManageAccountScreen, store);
    expect(asJSON).toMatchSnapshot();
  });

  it(`exports account`, async () => {
    // no network mock reset
    Sharing.shareAsync.mockImplementation(() => Promise.resolve());
    FileSystem.downloadAsync.mockImplementation(() => Promise.resolve());
    RemoteStorage.setAuthentication("thetoken");

    const store = createEmptyTestStore();
    const {getByLabelText, getByTestId, asJSON, debug} = renderScreenForTest(ManageAccountScreen, store);
    await waitForElement(() => getByLabelText("Meine Daten anfordern"));
    jest.useFakeTimers();
    fireEvent.press(getByLabelText("Meine Daten anfordern"));
    await jest.runAllTimers();
    expect(FileSystem.downloadAsync.mock.calls.length).toBe(1);
    expect(FileSystem.downloadAsync.mock.calls[0][0]).toBe(RemoteStorage.getBaseUrl() + "/account/data");
    const fileUrl = FileSystem.downloadAsync.mock.calls[0][1];
    expect(FileSystem.downloadAsync.mock.calls[0][2]["headers"]).toEqual({"Authorization": "Token thetoken"});


    expect(Sharing.shareAsync.mock.calls.length).toBe(1);
    expect(Sharing.shareAsync.mock.calls[0][0]).toBe(fileUrl);
  });
});