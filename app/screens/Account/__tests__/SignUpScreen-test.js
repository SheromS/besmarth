import React from "react";
import {createEmptyTestStore, renderScreenForTest} from "../../../test/test-utils";
import SignUpScreen from "../SignUpScreen";


describe("SignUpScreen", () => {
  it(`renders`, () => {
    const {asJSON} = renderScreenForTest(SignUpScreen, createEmptyTestStore());
    expect(asJSON()).toMatchSnapshot();
  });
});