import React from "react";
import {FlatList, LayoutAnimation, Platform, StyleSheet, UIManager, View} from "react-native";
import {bindActionCreators} from "redux";
import {fetchChallenges} from "../services/Challenges";
import {connect} from "react-redux";
import ChallengeListItem from "../components/ChallengeListItem";
import {Caption, IconButton, Text} from "react-native-paper";
import {ChallengesFilter} from "../components/ChallengesFilter";
import theme from "../themes/theme";
import debounce from "debounce";
import {notNull} from "../services/utils";

if (Platform.OS === "android") {
  if (UIManager.setLayoutAnimationEnabledExperimental) {
    UIManager.setLayoutAnimationEnabledExperimental(true);
  }
}

class ChallengesScreen extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      loading: true
    };
  }

  onToggleFilter() {
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);

    const newState = !this.state.filterVisible;
    this.setState({
      ...this.state,
      filterVisible: newState
    });
    this.props.navigation.setParams({filterVisible: newState});
  }

  componentDidMount() {
    this.subscription = this.props.navigation.addListener("focus", this.reload.bind(this));
  }

  componentWillUnmount() {
    this.subscription();
  }

  /**
   * Loads all challenges for the current topic from the server, applies the current filter and then updates the state
   */
  async reload() {
    try {
      this.props.navigation.setOptions({
        title: this.props?.route?.params?.topicName + " Challenges",
        headerRight: () => (
          <IconButton
            onPress={this.onToggleFilter.bind(this)}
            icon="filter"
            testID="toggle-challenges-filter"
            color={this.props?.route?.params?.filterVisible ? theme.colors.primary : "grey"}
            size={20}/>
        )
      });
      const challenges = await fetchChallenges(this.props.currentTopicId);
      const filteredChallenges = this.applyFilter(challenges, this.state.filter);
      this.setState({
        ...this.state,
        loading: false,
        allChallenges: challenges,
        filteredChallenges
      });
    } catch (e) {
      console.error(e);
      this.setState({
        ...this.state,
        error: JSON.stringify(e),
        loading: false
      });
    }
  }

  /**
   * Filters the passed challenges according to the filter config
   */
  applyFilter(challenges, filterConfig) {
    if (notNull(filterConfig) && (notNull(filterConfig.query) || notNull(filterConfig.category) || notNull(filterConfig.difficulty))) {
      return challenges.filter(challenge => {
        if (notNull(filterConfig.query) && !challenge.title.toLowerCase().includes(filterConfig.query)) {
          return false;
        }
        if (notNull(filterConfig.category) && challenge.category !== filterConfig.category) {
          return false;
        }
        if (notNull(filterConfig.difficulty) && challenge.difficulty !== filterConfig.difficulty) {
          return false;
        }
        return true;
      });
    } else {
      return challenges;
    }
  }

  /**
   * Applies the filter to the loaded challenges and updates the state.
   * Does not RELOAD from server
   */
  onFilter(filter) {
    this.setState({
      ...this.state,
      loading: true
    });
    const filteredChallenges = this.applyFilter(this.state.allChallenges, filter);
    this.setState({
      ...this.state,
      loading: false,
      filter,
      filteredChallenges
    });
  }

  render() {
    if (this.state.error) {
      return <View style={styles.centeredContainer}><Text>{this.state.error}</Text></View>;
    }
    const challenges = this.state.filteredChallenges;

    let emptyInfo;
    if (notNull(this.state.filter)) {
      emptyInfo = <Caption style={styles.emptyInfo}>
        Keine Challenges passen zu deinen Filterkriterien.
      </Caption>;
    } else {
      emptyInfo = <Caption style={styles.emptyInfo}>
        Zu diesem Thema gibt es keine Challenges...
      </Caption>;
    }

    return (
      <View style={styles.container}>
        {/* call this.onFilter only at max. all 0.5s */}
        <ChallengesFilter visible={this.state.filterVisible} onFilter={debounce(this.onFilter.bind(this), 500)}/>
        <FlatList
          testID="challenge-list"
          data={challenges}
          refreshing={this.state.loading}
          onRefresh={this.reload.bind(this)}
          ListEmptyComponent={() => emptyInfo}
          renderItem={({item}) => <ChallengeListItem
            challenge={item}></ChallengeListItem>}
          keyExtractor={item => item.id + ""}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  centeredContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  emptyInfo: {
    marginVertical: 10,
    marginHorizontal: 8,
    textAlign: "center"
  }
});

const mapStateToProps = state => {
  return {
    currentTopicName: state.topics.currentTopicName,
    currentTopicId: state.topics.currentTopicId
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators({}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(ChallengesScreen);
