import React from "react";
import {Image, StyleSheet, TouchableHighlight, View} from "react-native";
import {connect} from "react-redux";

import {bindActionCreators} from "redux";
import {Surface, Text} from "react-native-paper";
import Navigation from "../services/Navigation";
import bstyles from "../themes/besmarth-style";
import TopicLevel from "../components/TopicLevel";

class TopicScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
    };
  }

  componentDidMount() {
    this.props.navigation.setOptions({title: this.props.currentTopicName});
  }

  navigateToChallenges(topicId) {
    Navigation.push("HomeTab", "Challenges", {topicName: this.props.currentTopicName});
  }

  render() {
    const image = this.props.currentTopicImage;
    return (
      <View style={styles.container}>
        <View>
          <TopicLevel topicId={this.props.currentTopicId} size="100" textColor="black" showProgressBar/>
        </View>
        <View style={styles.containerTop}>
          <Image source={{uri: image}} style={styles.topicImage}/>
          <Surface style={styles.topicOverlay}>
          </Surface>
          <Text style={styles.desc}>{this.props.currentTopicDescription}</Text>
        </View>
        <View style={styles.containerBottom}>
          <TouchableHighlight style={[styles.rectButton1, styles.button]}
                              onPress={() => this.navigateToChallenges(this.props.currentTopicId)}>
            <Text style={styles.buttonText}>Challenges</Text>
          </TouchableHighlight>

          <TouchableHighlight style={[styles.rectButton2, styles.button]}
                              onPress={() => alert("Stay tuned, feature coming soon!")}>
            <Text style={styles.buttonText}>Gerettete Bäume</Text>
          </TouchableHighlight>
        </View>
        <View style={styles.containerBottom}>
          <TouchableHighlight style={[styles.rectButton3, styles.button]}
                              onPress={() => alert("Stay tuned, feature coming soon!")}>
            <Text style={styles.buttonText}>News</Text>
          </TouchableHighlight>

          <TouchableHighlight style={[styles.rectButton4, styles.button]}
                              onPress={() => alert("Stay tuned, feature coming soon!")}>
            <Text style={styles.buttonText}>Events</Text>
          </TouchableHighlight>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 10,
  },
  containerTop: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-between",
  },
  containerBottom: {
    flex: 1,
    flexDirection: "row"
  },
  topicImage: {
    borderRadius: 4,
    position: "relative",
    top: 0,
    left: 0,
    height: "100%",
    width: "100%",
    margin: 0,
    backgroundColor: "black"
  },
  buttonText: {
    color: "#DCE8CF",
    fontSize: 18,
  },
  button: {
    borderColor: "#3C591E",
    backgroundColor: "#829E65",
    justifyContent: "center", //center Text
    alignItems: "center",
    //Shadow:
    shadowColor: "#000",
    shadowOffset: {width: 0, height: 4,},
    shadowOpacity: 0.32,
    shadowRadius: 5.46,
    elevation: 9,
    borderWidth: 2,
    borderRadius: 4,
    padding: 8,
  },
  rectButton1: {
    width: "48%",
    marginRight: "2%",
    marginBottom: "2%",
    marginTop: "4%"
  },
  rectButton2: {
    width: "48%",
    marginLeft: "2%",
    marginBottom: "2%",
    marginTop: "4%"
  },
  rectButton3: {
    width: "48%",
    marginRight: "2%",
    marginBottom: "4%",
    marginTop: "2%",
  },
  rectButton4: {
    width: "48%",
    marginLeft: "2%",
    marginBottom: "4%",
    marginTop: "2%",
  },
  desc: {
    position: "absolute",
    top: "30%",
    left: 0,
    width: "100%",
    textAlign: "center",
    color: "#DCE8CF",
    fontSize: 16,
  },
  topicOverlay: {
    borderRadius: 4,
    position: "absolute",
    left: 0,
    bottom: 0,
    paddingHorizontal: 10,
    paddingVertical: 10,
    paddingBottom: 70,
    display: "flex",
    alignItems: "center",
    justifyContent: "flex-start",
    width: "100%",
    height: "100%",
    backgroundColor: "rgba(0,0,0,0.6)",
  },
});

const mapStateToProps = state => {
  return {
    currentTopicId: state.topics.currentTopicId,
    currentTopicName: state.topics.currentTopicName,
    currentTopicDescription: state.topics.currentTopicDescription,
    currentTopicImage: state.topics.currentTopicImage
  };
};

const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(TopicScreen);
