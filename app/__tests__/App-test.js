import React from "react";
import {act, create} from "react-test-renderer";

import App from "../App";
import LocalStorage from "../services/LocalStorage";

jest.mock("expo", () => ({
  AppLoading: "AppLoading",
}));

describe("App", () => {
  it(`renders the loading screen`, () => {
    const tree = create(<App />).toJSON();
    expect(tree).toMatchSnapshot();
  });

  it(`renders the error screen`, () => {
    const tree = create(<App/>);
    tree.root.instance.handleLoadingError("Test error");
    expect(tree.toJSON()).toMatchSnapshot();
  });

  it(`renders the root with loading screen`, async (done) => {
    let tree = create(<App/>);
    await act(async () => {
      try {
        await tree.root.instance.loadResourcesAsync();
        tree.root.instance.handleFinishLoading();
        expect(tree.toJSON()).toMatchSnapshot();
        done();
      } catch (e) {
        console.log(e);
        fail("errror", e);
      }
    });
  });

  it(`renders the app with loading screen in development mode`, async (done) => {
    window.__TESTMODE__ = false;
    const tree = create(<App />);
    await act(async () => {
      try {
        await tree.root.instance.loadResourcesAsync();
        tree.root.instance.handleFinishLoading();
        expect(tree.toJSON()).toMatchSnapshot();
        done();
        window.__TESTMODE__ = true;
      } catch (e) {
        console.log(e);
        fail("errror", e);
        window.__TESTMODE__ = true;
      }
    });
  });

  it(`renders the app with initial redux state`, async (done) => {
    window.__TESTMODE__ = false;
    await LocalStorage.setCacheItem("dev-persisted-redux-store", {
      app: {
        loading: true,
        error: "if you see this, state was not cleared"
      }
    });
    const app = create(<App/>);
    await act(async () => {
      try {
        await app.root.instance.loadResourcesAsync();
        app.root.instance.handleFinishLoading();
        expect(app.toJSON()).toMatchSnapshot();
        done();
        window.__TESTMODE__ = true;
      } catch (e) {
        console.log(e);
        fail("errror", e);
        window.__TESTMODE__ = true;
      }
    });
  });
});
