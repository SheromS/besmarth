import React from "react";
import AppToast from "../AppToast";
import {createEmptyTestStore, renderForSnapshot} from "../../test/test-utils";

describe(`AppToast`, () => {
  it(`renders with visible toast`, () => {
    const store = createEmptyTestStore();
    store.dispatch({
      type: "SHOW_TOAST",
      message: "This is the test message the user should see"
    });
    const tree = renderForSnapshot(<AppToast/>, store);
    expect(tree).toMatchSnapshot();
  });

  it(`renders with error toast`, () => {
    const store = createEmptyTestStore();
    store.dispatch({
      type: "SHOW_TOAST",
      toastType: "ERROR",
      message: "This is the test message the user should see"
    });
    const tree = renderForSnapshot(<AppToast/>, store);
    expect(tree).toMatchSnapshot();
  });

  it(`renders with invisible toast`, () => {
    const store = createEmptyTestStore();
    // test reducer
    store.dispatch({
      type: "SHOW_TOAST",
      message: "This is the test message the user should see"
    });
    store.dispatch({
      type: "HIDE_TOAST"
    });
    const tree = renderForSnapshot(<AppToast/>, store);
    expect(tree).toMatchSnapshot();
  });
});