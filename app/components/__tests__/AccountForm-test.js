import React from "react";
import AccountForm from "../AccountForm";
import {
  createEmptyTestStore,
  createNetworkMock,
  renderForSnapshot,
  renderForTest,
  waitForReduxAction
} from "../../test/test-utils";
import {fireEvent, waitForElement} from "@testing-library/react-native";
import RemoteStorage from "../../services/RemoteStorage";


function prepareAccount(store) {
  store.dispatch({
    type: "ACCOUNT_UPDATE",
    account: {
      username: "test",
      email: "test@email.com",
      first_name: "Samuel",
      token: "testtoken"
    }
  });
}

const mock = createNetworkMock();

describe("AccountForm", () => {
  it(`renders sign up mode`, () => {
    const store = createEmptyTestStore();
    const tree = renderForSnapshot(<AccountForm signUpMode={true} submitTitle="Speichern"/>, store).toJSON();
    expect(tree).toMatchSnapshot();
  });

  it(`renders manage account mode`, () => {
    const store = createEmptyTestStore();
    prepareAccount(store);
    const tree = renderForSnapshot(<AccountForm signUpMode={false} submitTitle="Speichern"/>, store).toJSON();
    expect(tree).toMatchSnapshot();
  });

  it(`shows alert when toc not acccepted in sign up mode`, async () => {
    window.alert = jest.fn();
    const store = createEmptyTestStore();
    const {getByLabelText, asJSON} = renderForTest(<AccountForm signUpMode={true} submitTitle="Speichern"/>, store);

    // do not check ToC

    const firstName = await waitForElement(() => getByLabelText("Vorname"));
    fireEvent.changeText(firstName, "Samuel");
    const username = await waitForElement(() => getByLabelText("Benutzername"));
    fireEvent.changeText(username, "test");
    const email = await waitForElement(() => getByLabelText("E-Mail"));
    fireEvent.changeText(email, "test@email.com");
    const password = await waitForElement(() => getByLabelText("Neues Passwort"));
    fireEvent.changeText(password, "fdef2134");

    const submitBtn = await waitForElement(() => getByLabelText("Speichern"));
    fireEvent.press(submitBtn);

    expect(window.alert.mock.calls.length).toBe(1);
    expect(asJSON());
  });

  it(`shows alert when no password set in sign up mode`, async () => {
    window.alert = jest.fn();
    const store = createEmptyTestStore();
    const {getByLabelText} = renderForTest(<AccountForm signUpMode={true} submitTitle="Speichern"/>, store);

    const acceptToC = await waitForElement(() => getByLabelText("Hiermit stimmst du unseren allgemeinen Geschäftsbedingungen zu."));
    acceptToC.props.onValueChange(true);
    const firstName = await waitForElement(() => getByLabelText("Vorname"));
    fireEvent.changeText(firstName, "Samuel");
    const username = await waitForElement(() => getByLabelText("Benutzername"));
    fireEvent.changeText(username, "test");
    const email = await waitForElement(() => getByLabelText("E-Mail"));
    fireEvent.changeText(email, "test@email.com");

    // do net set a password

    const submitBtn = await waitForElement(() => getByLabelText("Speichern"));
    fireEvent.press(submitBtn);

    expect(window.alert.mock.calls.length).toBe(1);
  });

  it(`submits data to endpoint`, async () => {
    // window.alert = jest.fn();
    mock.reset();
    mock.onPatch("/account", {
      acceptTermsOfService: true,
      pseudonymous: false,
      first_name: "Samuel",
      username: "test",
      email: "test@email.com",
      password: "fdef2134"
    }).reply(200, {
      username: "test",
      email: "test@email.com",
      password: "fdef2134",
      first_name: "Samuel",
      token: "testtoken"
    });
    const store = createEmptyTestStore();
    const {getByLabelText, asJSON} = renderForTest(<AccountForm signUpMode={true} submitTitle="Speichern"/>, store);

    const acceptToC = await waitForElement(() => getByLabelText("Hiermit stimmst du unseren allgemeinen Geschäftsbedingungen zu."));
    acceptToC.props.onValueChange(true);
    // fireEvent.change(acceptToC, { nativeEvent: true });
    const firstName = await waitForElement(() => getByLabelText("Vorname"));
    fireEvent.changeText(firstName, "Samuel");
    const username = await waitForElement(() => getByLabelText("Benutzername"));
    fireEvent.changeText(username, "test");
    const email = await waitForElement(() => getByLabelText("E-Mail"));
    fireEvent.changeText(email, "test@email.com");
    const password = await waitForElement(() => getByLabelText("Neues Passwort"));
    fireEvent.changeText(password, "fdef2134");

    const submitBtn = await waitForElement(() => getByLabelText("Speichern"));
    fireEvent.press(submitBtn);

    await waitForReduxAction("SHOW_TOAST");
    expect(mock.history.patch.length).toBe(1);
    expect(store.getState().app.toastMessage).toBe("Account erfolgreich angelegt");
    expect(asJSON()).toMatchSnapshot();
  });

  it(`handles error with an error box`, async () => {
    mock.reset();
    mock.onPatch("/account",).reply(400, {errors: "There was an error"});
    const store = createEmptyTestStore();
    const {getByLabelText, getByTestId, asJSON} = renderForTest(<AccountForm signUpMode={true}
                                                                             submitTitle="Speichern"/>, store);

    const acceptToC = await waitForElement(() => getByLabelText("Hiermit stimmst du unseren allgemeinen Geschäftsbedingungen zu."));
    acceptToC.props.onValueChange(true);
    // fireEvent.change(acceptToC, { nativeEvent: true });
    const firstName = await waitForElement(() => getByLabelText("Vorname"));
    fireEvent.changeText(firstName, "Samuel");
    const username = await waitForElement(() => getByLabelText("Benutzername"));
    fireEvent.changeText(username, "test");
    const email = await waitForElement(() => getByLabelText("E-Mail"));
    fireEvent.changeText(email, "test@email.com");
    const password = await waitForElement(() => getByLabelText("Neues Passwort"));
    fireEvent.changeText(password, "fdef2134");

    const submitBtn = await waitForElement(() => getByLabelText("Speichern"));
    fireEvent.press(submitBtn);

    await waitForElement(() => getByTestId("Fehleranzeige"));
    expect(mock.history.patch.length).toBe(1);
    expect(asJSON()).toMatchSnapshot();
  });

  it(`deletes account`, async () => {
    const store = createEmptyTestStore();
    prepareAccount(store);

    mock.onDelete("/account").reply(200, {});
    mock.onPost("/account", {pseudonymous: true}).reply(200, {
      username: "anothername",
      email: "anotheremail@email.com",
      first_name: "New pseudonymous account",
      token: "thenewtoken"
    });

    const {getByLabelText} = renderForTest(<AccountForm signUpMode={false} submitTitle="Test"/>, store);
    await waitForElement(() => getByLabelText("Account löschen"));
    fireEvent.press(getByLabelText("Account löschen"));

    await waitForElement(() => getByLabelText("Account löschen bestätigen"));
    fireEvent.press(getByLabelText("Account löschen bestätigen"));
    await waitForReduxAction("ACCOUNT_UPDATE");
    expect(store.getState().account).toEqual({
      loading: false,
      account: {
        username: "anothername",
        email: "anotheremail@email.com",
        first_name: "New pseudonymous account",
        token: "thenewtoken"
      }
    });
    expect(mock.history.delete.length).toBe(1);
    expect(RemoteStorage.API.headers["Authorization"]).toBe("Token thenewtoken");
  });
})
;