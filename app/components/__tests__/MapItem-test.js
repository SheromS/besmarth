import React from "react";
import {
  createEmptyTestStore,
  createNetworkMock,
  renderForSnapshot,
  renderForTest,
  waitForReduxAction
} from "../../test/test-utils";
import {fireEvent, waitForElement} from "@testing-library/react-native";
import MapItem from "../MapItem";
import Navigation from "../../services/Navigation";

const mock = createNetworkMock();

const topic4 = {
  "imageUrl": "/media/images/topic-levels/level02.png",
  "level": 2,
  "levelDescription": "Klimasupporter",
  "score": 0,
  "scoreNextLevel": 5,
  "scorePrevLevel": 1,
};

describe("MapItemTest", () => {
  it(`renders`, async () => {
    mock.reset();
    mock.onGet("/topics/4/progress").reply(200, [topic4]);

    const store = createEmptyTestStore();

    const {getByTestId, asJSON} = renderForTest(<MapItem topic={{
      "id": 4,
      "internal_id": 3,
      "topic_name": "Essen",
      "image_level1": "http://localhost:8000/media/images/topics/megan-thomas-xMh_ww8HN_Q-unsplash_kyFmbg4.jpg",
      "image_level2": null,
      "image_level3": null,
      "image_level4": null,
      "image_level5": null
    }}/>, store);

    await waitForElement(() => getByTestId("view"));
    expect(asJSON()).toMatchSnapshot();
  });

  it(`clicks on button navigates to another page`, () => {
    Navigation.navigate.mockClear();
    const {getByLabelText} = renderForTest(<MapItem topic={{
      "id": 4,
      "internal_id": 3,
      "topic_name": "Essen",
      "image_level1": "http://localhost:8000/media/images/topics/megan-thomas-xMh_ww8HN_Q-unsplash_kyFmbg4.jpg",
    }}/>, createEmptyTestStore());

    fireEvent.press(getByLabelText("Jetzt retten"));
    expect(Navigation.navigate).toHaveBeenCalledTimes(1);
    expect(Navigation.navigate.mock.calls[0][0]).toBe("HomeTab");
    expect(Navigation.navigate.mock.calls[0][1]).toBe("TopicDetail");
  });

  it(`clicks on large image navigates to another page`, () => {
    Navigation.navigate.mockClear();
    const {getByLabelText} = renderForTest(<MapItem topic={{
      "id": 4,
      "internal_id": 3,
      "topic_name": "Essen",
      "image_level1": "http://localhost:8000/media/images/topics/megan-thomas-xMh_ww8HN_Q-unsplash_kyFmbg4.jpg",
    }}/>, createEmptyTestStore());

    fireEvent.press(getByLabelText("Bild: Jetzt retten"));
    expect(Navigation.navigate).toHaveBeenCalledTimes(1);
    expect(Navigation.navigate.mock.calls[0][0]).toBe('HomeTab');
    expect(Navigation.navigate.mock.calls[0][1]).toBe('TopicDetail');
  });

  it(`changes current topic`, async () => {
    Navigation.navigate.mockClear();
    const store = createEmptyTestStore();
    const {getByLabelText} = renderForTest(<MapItem topic={{
      "id": 4,
      "internal_id": 3,
      "topic_name": "Essen",
      "image_level1": "http://localhost:8000/media/images/topics/megan-thomas-xMh_ww8HN_Q-unsplash_kyFmbg4.jpg"
    }}/>, store);

    const btn = await waitForElement(() => getByLabelText("Jetzt retten"));
    fireEvent.press(btn);
    // dont need to wait for anything as the button is not triggering async work
    expect(Navigation.navigate.mock.calls[0][0]).toBe('HomeTab');
    expect(Navigation.navigate.mock.calls[0][1]).toBe('TopicDetail');
    expect(store.getState().topics.currentTopicId).toBe(4);
    expect(store.getState().topics.currentTopicName).toBe("Essen");
  });
})
;