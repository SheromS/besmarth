import {DataTable, Headline, Paragraph} from "react-native-paper";
import {Image, StyleSheet, View} from "react-native";
import React from "react";
import {Colors, Typography} from "styles/index"


/**
 * Displays information about a challenge.
 * Is not connected with the redux store
 * Use like <ChallengeInfo challenge={yourChallenge} />
 */
export class ChallengeInfo extends React.Component {

  computeDifficultyStyle(challenge) {
    switch (challenge.difficulty) {
      case "ADVANCED":
        return Typography.advancedLevel;
        break;
      case "HARD":
        return Typography.hardLevel;
        break;
      case "EASY":
      default:
        return Typography.easyLevel;
        break;
    }
  }

  periodicityToString(periodicity) {
    switch (periodicity.toLowerCase()) {
      case "daily":
        return "Täglich";
      case "weekly":
        return "Wöchentlich";
      case "monthly":
        return "Monatlich";
      default:
        return "<Unknown periodicity " + periodicity;
    }
  }

  periodicityToUnitString(periodicity) {
    switch (periodicity.toLowerCase()) {
      case "daily":
        return "Tage";
      case "weekly":
        return "Wochen";
      case "monthly":
        return "Monate";
      default:
        return "<Unknown periodicity " + periodicity;
    }
  }

  render() {
    const challenge = this.props.challenge;
    const difficultyStyle = this.computeDifficultyStyle(challenge);

    return <View>
      <Image source={{uri: challenge.image}} style={styles.challengeImage}/>
      <View style={styles.contentWrapper}>
        <Headline style={Typography.descTitle}>{challenge.title}</Headline>
        <Paragraph style={Typography.descriptionText}>{challenge.description}</Paragraph>
      </View>
      <DataTable>
        <DataTable.Header>
          <DataTable.Title>Schwierigkeitsgrad</DataTable.Title>
          <DataTable.Title numeric>Frequenz</DataTable.Title>
          <DataTable.Title numeric>Dauer</DataTable.Title>
        </DataTable.Header>

        <DataTable.Row>
          <DataTable.Cell><Paragraph style={difficultyStyle}>{challenge.difficulty}</Paragraph></DataTable.Cell>
          <DataTable.Cell numeric>{this.periodicityToString(challenge.periodicity)}</DataTable.Cell>
          <DataTable.Cell
            numeric>{challenge.duration} {this.periodicityToUnitString(challenge.periodicity)}</DataTable.Cell>
        </DataTable.Row>
      </DataTable>
    </View>;
  }
}

const styles = StyleSheet.create({

  contentWrapper: {
    paddingHorizontal: 8,
    paddingVertical: 4
  },
  challengeImage: {
    height: 150,
    width: "100%",
  },
  card: {
    marginVertical: 4,
    marginHorizontal: 8,
  },
});
