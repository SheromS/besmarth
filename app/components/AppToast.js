import React from "react";
import {Snackbar} from "react-native-paper";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import {hideToast} from "../services/Toast";

/**
 * Shows a toast depending on state
 */
class AppToast extends React.Component {
  render() {
    let backgroundColor, textColor;
    if (this.props.toastType === "ERROR") {
      backgroundColor = "#d32f2f";
      textColor = "black";
    } else {
      backgroundColor = "white";
      textColor = "black";
    }

    return (
      <Snackbar
        visible={this.props.toastVisible}
        onDismiss={() => this.props.hideToast()}
        duration={this.props.toastType ? 5000 : 2000}
        style={{marginBottom: 60, left: 0, backgroundColor: backgroundColor}}
        //for more theme attributes: https://blog.logrocket.com/designing-a-ui-with-custom-theming-using-react-native-paper/
        theme={{colors: {accent: textColor, surface: textColor}}}
        action={{
          label: "OK",
          onPress: () => this.props.hideToast()
        }}
      >
        {this.props.toastMessage}
      </Snackbar>
    );
  }
}

const mapStateToProps = state => {
  return {
    toastVisible: state.app.toastVisible,
    toastMessage: state.app.toastMessage,
    toastType: state.app.toastType
  };
};

const mapDispatchToProps = dispatch => bindActionCreators({
  hideToast
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(AppToast);