 import NetInfo from "@react-native-community/netinfo";

/**
 * Returns true if the phone is online.
 * @returns {Promise<unknown>}
 */
export async function isPhoneOnline() {
  const state = await NetInfo.fetch();
  return state.isConnected;
}

/**
 * Returns true if the phone is connected to a wifi connection
 */
export async function isConnectedToWifi() {
  const state = NetInfo.fetch();
  return state.type.toLowerCase() === "wifi";
}