import {isDevelopmentMode, notNull} from "./utils";
import RemoteStorage from "./RemoteStorage";
import {ACCOUNT_API_PATH, ACCOUNT_CACHE_KEY} from "./AccountService";
import * as SecureStore from "expo-secure-store";
import {isPhoneOnline} from "./phone-utils";

const DEVTOKEN_API_PATH = "/account/devtoken";

function dispatchAccount(dispatch, account) {
  dispatch({
    type: "ACCOUNT_UPDATE",
    account: account
  });
}

async function saveToPhone(account) {
  return SecureStore.setItemAsync(ACCOUNT_CACHE_KEY, JSON.stringify(account));
}

async function loadAccountFromPhone() {
  return JSON.parse(await SecureStore.getItemAsync(ACCOUNT_CACHE_KEY));
}

export function signIn(username, password) {
  return async (dispatch) => {
    const result = await RemoteStorage.post(ACCOUNT_API_PATH, {username, password});
    RemoteStorage.setAuthentication(result.data.token);
    // TODO ske remove all local data and load data from server
    dispatch({type: "ACCOUNT_UPDATE", account: result.data});
    await saveToPhone(result.data);
    return result.data;
  };
}

export async function signOut(that) {
  await SecureStore.deleteItemAsync(ACCOUNT_CACHE_KEY);
  RemoteStorage.setAuthentication(null);
}

/**
 * Creates a new account
 * @param dispatch
 * @returns {Promise<boolean>} true when everything worked, false if anything didnt work
 */
async function createNewAccount(dispatch) {
  console.log("Account information not found, creating a new pseudonymous account!");
  if (!await isPhoneOnline()) {
    dispatch({type: "APP_ERROR", error: "Du musst online sein wenn du die App das erste Mal benutzt."});
    return false;
  } else {
    try {
      const response = await RemoteStorage.post(ACCOUNT_API_PATH, {pseudonymous: true});
      const account = response.data;
      await saveToPhone(account);
      RemoteStorage.setAuthentication(account.token);
      dispatchAccount(dispatch, account);
      return true;
    } catch (error) {
      dispatch({
        type: "APP_ERROR",
        error: "Das Aufstarten ist fehlgeschlagen, bitte versuche es später noch einmal."
      });
      return false;
    }
  }
}

/**
 * Initializes authentication for the app:
 * @returns {Function}
 */
export function initAuth() {
  return async (dispatch, getState) => {
    let cachedAccount = await loadAccountFromPhone();
    if (isDevelopmentMode()) {
      cachedAccount = null;
    }
    // will ensure in development, always the admin user is taken
    if (isDevelopmentMode()) {
      let devTokenResponse;
      try {
        devTokenResponse = await RemoteStorage.get(DEVTOKEN_API_PATH);
      } catch (error) {
        dispatch({
          type: "APP_ERROR",
          error: JSON.stringify(error.data)
        });
        throw new Error(JSON.stringify(error, null, 2) || "error loading devtoken");
      }
      const token = devTokenResponse.data;
      RemoteStorage.setAuthentication(token);
      try {
        const response = await RemoteStorage.get(ACCOUNT_API_PATH);
        const account = response.data;
        dispatchAccount(dispatch, account);
        await saveToPhone(account);
        dispatch({
          type: "APP_LOADING_COMPLETE"
        });
        return account;
      } catch (error) {
        dispatch({
          type: "APP_ERROR",
          error: JSON.stringify(error.data || error)
        });
        throw new Error(e);
      }
    } else if (notNull(cachedAccount)) {
      RemoteStorage.setAuthentication(cachedAccount.token);
      dispatchAccount(dispatch, cachedAccount);
      dispatch({
        type: "APP_LOADING_COMPLETE"
      });
      return cachedAccount;
    } else {
      const success = await createNewAccount(dispatch);
      dispatch({
        type: "APP_LOADING_COMPLETE"
      });
      if (success) {
        return success;
      } else {
        throw new Error("error creating new account");
      }
    }
  };
}
