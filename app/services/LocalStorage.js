import * as SecureStore from "expo-secure-store";
import {AsyncStorage} from "react-native";
import {isDevelopmentMode} from "./utils";
import {ACCOUNT_CACHE_KEY} from "./AccountService";

/**
 * API for storing data on the phone
 */
export default class LocalStorage {
  static CACHE_KEY_PREFIX = "besmarth_cache.";

  static async clearCache(){
    await AsyncStorage.clear();
  }

  /**
   * Dumps the current cache
   */
  static async dumpCache() {
    const keys = await AsyncStorage.getAllKeys();
    const everything = await AsyncStorage.multiGet(keys);
    const map = {};
    everything.reduce((x, value) => {
      map[value[0]] = JSON.parse(value[1]);
    }, []);
    return map;
  }

  /**
   * Use this to retrieve any data from the phone cache
   *
   */
  static async getCacheItem(key) {
    const result = await AsyncStorage.getItem(LocalStorage.CACHE_KEY_PREFIX + key);
    return JSON.parse(result);
  }

  /**
   * Puts a cache item into the store
   * Cache items are not backed up on server
   */
  static async setCacheItem(key, value) {
    await AsyncStorage.setItem(LocalStorage.CACHE_KEY_PREFIX + key, JSON.stringify(value));
  }

  /**
   * Removes an item from the cache
   */
  static async deleteCacheItem(key) {
    await AsyncStorage.removeItem(LocalStorage.CACHE_KEY_PREFIX + key);
  }
}