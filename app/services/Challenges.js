import RemoteStorage from "./RemoteStorage";
import LocalStorage from "./LocalStorage";
import {notNull} from "./utils";
import {isPhoneOnline} from "./phone-utils";

const CHALLENGES_API_PATH = "/challenges/topic/";
export const CHALLENGES_CACHE_KEY = "challenges";

function buildChallengeParticipationPath(challengeId) {
  return "/challenges/" + challengeId + "/participation/";
}

function buildChallengeParticipationProgressPath(challengeId) {
  return "/challenges/" + challengeId + "/participation/progress/";
}

/**
 * Fetches all challenges of a topic from the server
 */
export async function fetchChallenges(topicId) {
  if (await isPhoneOnline()) {
    const response = await RemoteStorage.get(CHALLENGES_API_PATH + topicId + "/");
    const challenges = response.data;
    await LocalStorage.setCacheItem(CHALLENGES_CACHE_KEY, challenges);
    return challenges;
  } else {
    console.log("Device is offline, getting challenges from cache");
    const cachedChallenges = await LocalStorage.getCacheItem(CHALLENGES_CACHE_KEY);
    if (notNull(cachedChallenges)) {
      return cachedChallenges.filter(c => c.topic === topicId);
    } else {
      throw new Error("device is offline and no challenges are cached");
    }
  }
}

/**
 * Loads the progress of a single challenge into
 */
export async function fetchChallengeParticipation(challenge) {
  if (!await isPhoneOnline()) {
    throw new Error("no internet connection available");
  }

  try {
    const response = await RemoteStorage.get(buildChallengeParticipationPath(challenge.id));
    const participation = response.data;
    return participation;
  } catch (e) {
    // 404 happens when the user did not already signed up for the challenge
    if (e.status !== 404) {
      throw new Error("Unerwarteter Fehler beim Laden", e);
    } else {
      return null;
    }
  }
}

/**
 * Signs the user up to that challenge
 * @param challenge
 * @returns {Function}
 */
export async function signUpChallenge(challenge) {
  const response = await RemoteStorage.post(buildChallengeParticipationPath(challenge.id));
  const participation = response.data;
  return participation;
}

/**
 * Marks the challenge as 'done' for one time unit (day/week/month)
 */
export async function createChallengeProgress(challenge) {
  const response = await RemoteStorage.post(buildChallengeParticipationProgressPath(challenge.id));
  const participation = response.data;
  return participation;
}

export async function unfollowChallenge(challenge) {
  await RemoteStorage.delete(buildChallengeParticipationPath(challenge.id));
}

/**
 * Shares the challenge to the user's friends
 * @param challenge
 */
export async function changeSharedStateChallenge(challenge, shared) {
  const response = await RemoteStorage.patch(buildChallengeParticipationPath(challenge.id), {shared});
  const participation = response.data;
  return participation;
}

/**
 * Load challenges of a friend.
 * Returns an array of participations
 */
export async function loadFriendChallenges(friend_id) {
  const response = await RemoteStorage.get(`/account/friends/${friend_id}/challenges`);
  return response.data;
}

/**
 * Loads all challenges the user is currently taking part
 * @returns {Promise<unknown>}
 */
export async function fetchOwnChallengeParticipations() {
  const response = await RemoteStorage.get(`/account/participations`);
  return response.data;
}

/**
 * The log progress button is only visible if it was not already pressed the same day/week/month
 */
export function progressButtonAvailable(participation) {
  if (!notNull(participation?.progress) || participation.progress.length === 0) {
    return true;
  }

  return participation.progress_loggable;
}

export function getNumberOfCompletedChallenges(participation, challenge) {
  return Math.floor(((participation?.progress?.length || 0) / challenge.duration));
}
