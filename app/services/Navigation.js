import React from "react";
import {notNull} from "./utils";
import {CommonActions, StackActions} from "@react-navigation/native";

export const navigationRef = React.createRef();

function checkForExistence() {
  if (!notNull(navigationRef.current)) {
    console.error("Tried to use Navigation.js but the NavigationContainer was not yet known");
  }
}

/**
 * Navigates to a screen. When the screen is already open, it gets pushed on top the navigation stack.
 */
export function navigate(tabName, routeName, params = {}) {
  checkForExistence();
  if (!tabName.endsWith("Tab")) {
    console.warn(`You passed '${tabName}' which is not a top-level Tab (bottom tabs). This means you improperly called Navigation.navigate`);
  }
  navigationRef.current.navigate(tabName, {screen: routeName, params});
}

/**
 * Navigates to a new screen.
 * push() always open a new screen on the navigation stack.
 */
export function push(tabName, routeName, params) {
  checkForExistence();
  if (!tabName.endsWith("Tab")) {
    console.warn(`You passed '${tabName}' which is not a top-level Tab (bottom tabs). This means you improperly called Navigation.push`);
  }
  navigationRef.current.dispatch(CommonActions.navigate(tabName, {screen: routeName, params}));
}

export function goBack(){
  checkForExistence();
  navigationRef.current.goBack();
}

export default {navigate, push, goBack};