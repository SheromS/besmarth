import Constants from "expo-constants";
import {create} from "apisauce";
import {isDevelopmentMode, isProductionMode, notNull} from "./utils";


const {manifest} = Constants;

const BASE_URL = isProductionMode() ? manifest.extra.backend : `http://${(manifest.debuggerHost || "127.0.0.1:8000").split(":").shift()}:8000`;
console.log("Using backend: " + BASE_URL);

export default class RemoteStorage {

  static API = create({
    baseURL: BASE_URL,
    headers: {Accept: "application/json"},
    timeout: 1000
  });

  static getBaseUrl() {
    return BASE_URL;
  }

  /**
   * Set the authentification token use for authenticating
   * @param token
   */
  static setAuthentication(token) {
    if (isDevelopmentMode()) {
      console.log("Authentication token set to " + token);
    }
    if (notNull(token)) {
      RemoteStorage.API.setHeader("Authorization", "Token " + token);
    } else {
      RemoteStorage.API.deleteHeader("Authorization");
    }
  }

  /**
   * Checks if the server is reachable
   * @returns {Promise<void>}
   */
  static async healthCheck() {
    return new Promise((resolve, reject) => {
      this.get("/health").then((response) => {
        if (notNull(response) && notNull(response.data) && response.data.status === "online") {
          resolve(true, response);
        } else {
          reject(false, response);
        }
      }, error => {
        reject(false, error);
      });
    });
  }

  static get(path) {
    return RemoteStorage.API.get(path);
  };

  static post(path, data) {
    return RemoteStorage.API.post(path, data);
  };

  static put(path, data) {
    return RemoteStorage.API.put(path, data);
  };

  static patch(path, data) {
    return RemoteStorage.API.patch(path, data);
  };

  static delete(path) {
    return RemoteStorage.API.delete(path);
  };

}

// init code
RemoteStorage.API.addResponseTransform(response => {
  if (!response.ok) {
    throw response;
  }
});
