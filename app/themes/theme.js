import {DefaultTheme} from 'react-native-paper';


export default {
  ...DefaultTheme,
  roundness: 4,
  colors: {
    ...DefaultTheme.colors,
    primary: '#006B2A',
    accent: '#F1E54C',
    background: 'white'
  },

};
