import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  button: {
    marginVertical: 8,
    minWidth: "50%"
  }
});