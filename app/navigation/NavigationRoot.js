import React from "react";
import {fetchDailyTip, hasUserSeenDailyTip} from "../services/DailyTipService";
import {AsyncStorage, Platform, StyleSheet, View} from "react-native";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import {initAuth} from "../services/Auth";
import {isDevelopmentMode, notNull} from "../services/utils";
import Colors from "../constants/Colors";
import {ActivityIndicator, Button, Headline} from "react-native-paper";
import ErrorBox from "../components/ErrorBox";
import Navigation, {navigationRef} from "../services/Navigation";
import {NavigationContainer} from "@react-navigation/native";
import {createMaterialBottomTabNavigator} from "@react-navigation/material-bottom-tabs";
import LocalStorage from "../services/LocalStorage";
import TabBarIcon from "../components/TabBarIcon";
import {AccountStackScreen, FriendsStackScreen, HomeStackScreen, MyChallengesStackScreen} from "./NavigationStacks";

const persistenceKey = "navigation-dev-state";
const Tab = createMaterialBottomTabNavigator();

/**
 * Screen opened right after app opens.
 * It decides what the user will seen at startup.
 */
class NavigationRoot extends React.Component {
  constructor() {
    super();
    this.state = {
      ready: false
    };
  }

  componentDidMount() {
    (async () => {
      await this.props.initAuth();
      const navState = await this.loadNavDev();
      this.setState({ready: true, navState});
      await this.handleDailyTip();
    })();
  }

  async handleDailyTip() {
    try {
      const alreadySeen = await hasUserSeenDailyTip();
      if (!alreadySeen) {
        await this.props.fetchDailyTip();
        console.log("Navigating to daily tip");
        Navigation.push("HomeTab", "DailyTip");
      }
    } catch (e) {
      console.log("Daily tip not shown: ", e);
      // when failed to fetch daily tip, just show home screen (offline case)
    }
  }

  /**
   * Loads the stored navigation state (development mode only)
   */
  async loadNavDev() {
    if (isDevelopmentMode()) {
      return await LocalStorage.getCacheItem(persistenceKey);
    }
  }

  /**
   * Persists the nav state to the device's cache (development mode only)
   */
  async onNavStateChange(navState) {
    if (isDevelopmentMode()) {
      await LocalStorage.setCacheItem(persistenceKey, navState);
    }
  }

  render() {
    if (notNull(this.props.error)) {
      return (<View style={[styles.container, {backgroundColor: Colors.errorBackground}]}>
        <Headline style={styles.errorColor}>Fehler</Headline>
        <ErrorBox errors={{"error": [this.props.error]}}></ErrorBox>
        {isDevelopmentMode() ?
          <Button mode="contained" onPress={async () => await LocalStorage.clearCache()}>Clear LocalStorage</Button> : null}
      </View>);
    }

    const screenOptions = (label, iconIos, iconAndroid) => {
      const icon = Platform.OS === "ios" ? iconIos : iconAndroid;
      return ({route}) => ({
        tabBarLabel: label,
        tabBarIcon: ({focused, color, size}) => <TabBarIcon
          name={icon}
          focused={focused}/>
      });
    };

    if (this.props.loading || !this.state.ready) {
      return (
        <View style={styles.container}>
          <ActivityIndicator testID="nav-loading-indicator" animating={true} size={100}/>
        </View>
      );
    } else {
      return (
        <NavigationContainer ref={navigationRef}
                             onStateChange={this.onNavStateChange.bind(this)}
                             initialState={this.state.navState}>
          <Tab.Navigator
            backBehaviour="history"
            shifting={true}
            labeled={true}
            barStyle={{backgroundColor: "white"}}>
            <Tab.Screen name="HomeTab" component={HomeStackScreen}
                        options={screenOptions("Home", "ios-home", "md-home")}/>
            <Tab.Screen name="MyChallengesTab" component={MyChallengesStackScreen}
                        options={screenOptions("Challenges", "ios-speedometer", "md-speedometer")}/>
            <Tab.Screen name="FriendsTab" component={FriendsStackScreen}
                        options={screenOptions("Freunde", "ios-contacts", "md-contacts")}/>
            <Tab.Screen name="AccountTab" component={AccountStackScreen}
                        options={screenOptions("Account", "ios-contact", "md-contact")}/>
          </Tab.Navigator>
        </NavigationContainer>
      );
    }
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexGrow: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  errorColor: {
    color: Colors.errorText
  },
  errorText: {
    color: Colors.errorText,
    padding: 6
  }
});

const mapStateToProps = state => {
  return {
    loading: state.app.loading,
    error: state.app.error,
    signedIn: !state.account.account.pseudonymous,
  };
};

const mapDispatchToProps = dispatch => bindActionCreators({
  initAuth,
  fetchDailyTip
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(NavigationRoot);
