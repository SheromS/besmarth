import React from "react";
import {AppLoading} from "expo";
import {Asset} from "expo-asset";
import * as Font from "expo-font";
import {AsyncStorage, Platform, StatusBar, StyleSheet, View} from "react-native";
import {Ionicons} from "@expo/vector-icons";
import {Provider as PaperProvider} from "react-native-paper";

import {Provider as ReduxProvider} from "react-redux";
import theme from "./themes/theme";
import AppToast from "./components/AppToast";
import {isDevelopmentMode, notNull} from "./services/utils";
import LocalStorage from "./services/LocalStorage";
import initStore from "./redux/initStore";
import debounce from "debounce";
import NavigationRoot from "./navigation/NavigationRoot";
import {initialReduxState} from "./redux/reducers/app";
import * as SecureStore from "expo-secure-store";
import {ACCOUNT_CACHE_KEY} from "./services/AccountService";
import RewardToastService from "./services/RewardToastService";
import RewardToast from "./components/RewardToast";
import {Portal} from "react-native-paper";
import RemoteStorage from "./services/RemoteStorage";
import {checkHeader} from "./services/Reward";

RemoteStorage.API.addResponseTransform(response => {
  checkHeader(response.headers);
});

export default class App extends React.Component {
  state = {
    loading: true
  };

  render() {
    if (this.state.loading && !this.props.skipLoadingScreen) {
      return (
        <AppLoading
          startAsync={this.loadResourcesAsync.bind(this)}
          onError={this.handleLoadingError.bind(this)}
          onFinish={this.handleFinishLoading.bind(this)}
        />
      );
    } else {
      return (
        <View style={styles.container}>
          {Platform.OS === "ios" && <StatusBar barStyle="default"/>}
          <ReduxProvider store={this.state.reduxStore}>
            <PaperProvider theme={theme}>
              <AppToast/>
              <Portal.Host>
              <RewardToast/>
              <NavigationRoot/>
              </Portal.Host>
            </PaperProvider>
          </ReduxProvider>
        </View>
      );
    }
  }

  async initReduxStore() {
    const reduxPersistKey = "dev-persisted-redux-store";
    let initialState = initialReduxState;
    // load state from localstorage when in development mode
    if (isDevelopmentMode()) {
      initialState = await LocalStorage.getCacheItem(reduxPersistKey);
      if (notNull(initialState)) {
        // those properties might be loaded from cache but should not be "cached".
        initialState.app.loading = true;
        delete initialState.app.error;
      } else {
        initialState = initialReduxState;
      }
    }
    const store = initStore(initialState);
    this.setState({
      ...this.state,
      reduxStore: store
    });

    RewardToastService.setStoreForReward(store);

    // persist redux store in development mode
    if (isDevelopmentMode()) {
      store.subscribe(debounce(async () => {
        // store redux state with each update (but at most every 1s)
        await LocalStorage.setCacheItem(reduxPersistKey, store.getState());
      }, 1000));
    }
  }

  async loadResourcesAsync() {
    console.log("Initializing app...");
    const LAST_USAGE_KEY = "isLastUsageInDevEnv";
    const lastUsageInDev = await LocalStorage.getCacheItem(LAST_USAGE_KEY);
    // whenever the environment changes
    if (lastUsageInDev !== isDevelopmentMode()) {
      let before = lastUsageInDev ? "dev" : "prod";
      let now = isDevelopmentMode() ? "dev" : "prod";
      console.log("Deleting local storage due to environment change from " + before + " to " + now);
      // clear all local data
      await LocalStorage.clearCache();
      // clear cached account
      await SecureStore.deleteItemAsync(ACCOUNT_CACHE_KEY);
    }
    await LocalStorage.setCacheItem(LAST_USAGE_KEY, isDevelopmentMode());


    await Promise.all([
      this.initReduxStore(),
      Asset.loadAsync([]),
      Font.loadAsync({
        // This is the font that we are using for our tab bar
        ...Ionicons.font,
      }),
    ]);
  }

  handleLoadingError(error) {
    console.error("App loading ERROR!");
    console.error(error);
  }

  handleFinishLoading() {
    this.setState({
      ...this.state,
      loading: false
    });
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
  },
});
