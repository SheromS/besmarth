import React from "react";
import initStore from "../initStore";

describe("initStore test", () => {
  it(`initStore test`, () => {
    const store = initStore({
      app: {
        loading: true
      }
    });

    expect(store.getState()).toEqual({
      "account": {
        "account": {
          "email": null,
          "first_name": null,
          "pseudonymous": true,
          "username": null
        },
        "loading": false
      },
      "app": {
        "loading": true
      },
      "dailyTip": {
        "content": "Kein Tipp des Tages gefunden.",
        "date": "1990-10-05T22:19:34.550Z",
        "title": "Standard"
      },
      "topics": {
        "currentTopicDescription": "",
        "currentTopicId": -1,
        "currentTopicName": null,
        "refreshing": false,
        "topics": {}
      }
    });
  });
});
