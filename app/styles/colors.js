export const darkGray = '#494F55'
export const gray = '#838383'
export const lightGray = '#bdb9b9'
export const lightGreen = '#31b866'
export const green = '#006B2A'
export const yellow = '#F1E54C'
export const white = '#ffffff'
export const red = '#f8320b'



export const baseText = darkGray
export const descText = gray
export const sectionBackground = lightGray
export const background = white
export const selected = darkGray
export const unselected = green
export const easyLevel = lightGreen
export const advancedLevel = green
export const hardLevel = red
